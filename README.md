# README #

### How do I get set up? ###

* Go to which ever folder on your computer you want the project to be stored (i.e C:\myProjectFolder).
* If you got TortoiseGit installed you should be able to right click, Pull. Make use your location to Pull from is the one in the top right of this page, it should be something like: https://yourname@bitbucket.org/pblgro............
* It should ask you for your password, after you have entered it the project should be downloaded and you should be able to run it. 
* **Don't forget to change your username/email if its incorrect**, if it is incorrect go to GitGui->Edit->Options.
* From that point onwards you should be ok as it just GIT, then Push changed when you have finished them (make sure the changes will before you Push)
* Download the GroupProject.zip from Downloads in order to get all the other files (i.e VS2010 Project files) and place in your folder or above the Pulled folder.

For more information head over to the  [Wiki](wiki).