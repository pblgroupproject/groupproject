#version 330 ///< shader version

uniform sampler2D ps_texture; ///< texture
uniform vec4 ps_ambient; ///< ambient
uniform vec4 ps_diffuse; ///< diffuse
uniform vec4 ps_specular; ///< specular

// params passed by vertex shader
in vec3 ps_Light; ///< direction of the light
in vec3 ps_Eye; ///< eye direction, this will be easy as it will be the same direction as the light
in vec3 ps_Normal; ///< a normal vector
in vec2 ps_uv; ///< uv from vertex shader

out vec4 out_colour; ///< output colour

void main()
{
  // normalize vectors
  vec3 eye = normalize(ps_Eye);
  vec3 light = normalize(ps_Light);
  vec3 normal = normalize(ps_Normal);

  // compute reflected vector between the eye and normal
  vec3 reflect = -reflect(eye, normal);

  // compute dot product (N.L) between normal and light
  float N_L = max(dot(normal, light), 0.0);

  // compute ambient lighting term
  vec4 tA = ps_ambient;
  // compute diffuse lighting term
  vec4 tD = texture2D(ps_texture, ps_uv) * ps_diffuse * N_L;
  // compute specular lighting term
  vec4 tS = ps_specular * pow(max(dot(reflect, light), 0.0), 5.0); ///< might want to change last term to uniform (CosPower) to allow for more control!

  // output texture
  out_colour = tA + tD + tS; 
}