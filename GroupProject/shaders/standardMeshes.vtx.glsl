#version 330 ///< shader version

layout(location = 0) in vec4 vs_position; ///< the vertex co-odinate from VBO
layout(location = 1) in vec3 vs_normal; ///< the normal vector from VBO
layout(location = 2) in vec2 vs_uv; ///< the UV co-odinate from VBO

// uniform data
uniform mat4 vs_mvp;
uniform mat4 vs_mv;

// params passed on to fragment shader
out vec3 ps_Light; ///< direction of the light
out vec3 ps_Eye; ///< eye direction, this will be easy as it will be the same direction as the light
out vec3 ps_Normal; ///< a normal vector
out vec2 ps_uv; ///< send uv to fragment shader

void main()
{
  // send uv to fragment shader
  ps_uv = vs_uv;

  // transform normal to viewspace
  ps_Normal = (vs_mv * vec4(vs_normal, 0.0)).xyz;

  // compute vector from light to position, this could possibly be done a little better!
  vec4 light = vec4(0, 0, 0, 0) - vs_mv * normalize(vs_position);

  // pass light and eye vector to the fragment shader as vec3's
  ps_Light = light.xyz;
  ps_Eye = light.xyz;

  // position output
  gl_Position = vs_mvp * vs_position;
}