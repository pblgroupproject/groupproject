#version 330 ///< shader version

uniform sampler2D ps_texture; ///< texture
in vec2 ps_uv; ///< uv from vertex shader

out vec4 ps_colour; ///< output colour

void main()
{
	// output texture
	ps_colour = texture2D(ps_texture, ps_uv);
}