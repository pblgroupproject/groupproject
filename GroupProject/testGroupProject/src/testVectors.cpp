#include "testGroupProject.h"
//------------------------------------------
// add one of these for each member func!
//------------------------------------------
TEST(Vec2, Add)
{
 vec2 a = vec2(5, 10);
 vec2 b = vec2(6, 11);
 EXPECT_EQ(vec2(11, 21), a + b);
}