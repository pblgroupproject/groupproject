#include "Texture.h"

Texture::Texture()
{
	
}

Texture::~Texture()
{
	// delete textures
	glDeleteTextures(1, &m_textures);
}

void Texture::LoadBMP(std::string filename)
{
	// Converts a SDL Surface to an OpenGL Texture
	// This following code has been taken from http://content.gpwiki.org/index.php/SDL:Tutorials:Using_SDL_with_OpenGL
	if(m_surf = SDL_LoadBMP(filename.c_str()))
	{
		// Error Checking - makes sure the images pixel width and height is divisable by 2 i.e. 512 x 512
		if((m_surf->w & (m_surf->w - 1)) != 0)
		{
			std::cout << "image width not a power of 2" << std::endl;
		}

		if((m_surf->h & (m_surf->h - 1)) != 0)
		{
			std::cout << "image height not a power of 2" << std::endl;
		}
		//Finds the number of channels the surface has
		m_colourNum = m_surf->format->BytesPerPixel;
		if(m_colourNum == 4)
		{
			if(m_surf->format->Rmask == 0x000000ff) // Alpha Channel
			{
				m_texFormat = GL_RGBA;
			}
			else
			{
				m_texFormat = GL_BGRA;
			}
		}

		else if(m_colourNum == 3)
		{
			if(m_surf->format->Rmask = 0x000000ff) // No Alpha Channel
			{
				m_texFormat = GL_BGR;				
			}
			else
			{
				m_texFormat = GL_RGB;
			}
		}

		else // Number of Channels in the image shouldn't be anything other than 3 or 4
		{
			std::cout << "This image is not a truecolor image" << std::endl;
		}
			// get width and height of image
			m_height = m_surf->h;
			m_width = m_surf->w;

			// initialize our textures generate one texture and pass in textures variable. This will, providing there are no errors, 
			// give us an array which the names of the generated textures are stored ---- 1 equals num of textures
			glGenTextures(1, &m_textures);
			// bind our 2D texture for modification
			glBindTexture(GL_TEXTURE_2D, m_textures);
			
			// texture parameters
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			// now we specify our 2D texture image with various parameters (i.e width and height)
			glTexImage2D(GL_TEXTURE_2D, 0, m_colourNum, m_width, m_height, 0, m_texFormat, GL_UNSIGNED_BYTE, m_surf->pixels);
			// unbind our texture ny binding to texture '0'
			glBindTexture(GL_TEXTURE_2D, 0);
	}
	else
	{
		std::cout << "SDL cannot load image :(" << std::endl;
		return;
	}

	if(m_surf) // Frees surface after it has been created without any problems
	{
		SDL_FreeSurface(m_surf);
	}
}

void Texture::bind(int slot)
{
	// bind the buffer
	glActiveTexture(GL_TEXTURE0+slot);
	glBindTexture(GL_TEXTURE_2D, m_textures); 
}
void Texture::unbind()
{
	// unbind buffer
	glBindTexture(GL_TEXTURE_2D, 0);
}
