#include "mg_Plane.h"

mg_Plane::mg_Plane(float height, float width)
{
  // create classes for minigames
  m_player = new Plane(height, width);
}
mg_Plane::~mg_Plane()
{
  // cleanup
  delete m_player;
}
bool mg_Plane::update(int& EventCode, float& dt)
{
  // update classes
	return m_player->update(EventCode, dt);
}
bool mg_Plane::update(float& dt, vec2& kinectLeftHand, vec2& kinectRightHand)
{
	// update classes
	return m_player->update(dt, kinectLeftHand, kinectRightHand);
}
void mg_Plane::draw()
{
  // draw
  m_player->draw();
}
int mg_Plane::gameScore()
{
	return m_player->score();
}
 