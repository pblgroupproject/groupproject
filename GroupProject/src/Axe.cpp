#include "Axe.h"

Axe::Axe(float height, float width)
{
  // setting member variables
  m_height = height;
  m_width = width;
  m_texture[0] = new Texture;
	m_texture[1] = new Texture;
	m_texture[2] = new Texture;
  m_texture[0]->LoadBMP("images/basicAxe.bmp");
	m_texture[1]->LoadBMP("images/log.bmp");
	m_texture[2]->LoadBMP("images/AxeSky.bmp");
  m_mesh[0] = new ModelLoader("meshes/Axe/basicAxe.meshes");
	m_mesh[1] = new ModelLoader("meshes/logSmall.meshes");
	m_mesh[2] = new ModelLoader("meshes/log.meshes");
	m_mesh[3] = new ModelLoader("meshes/skyPlane.meshes");

  // load shader source code
	char* vertexSource = ogl::loadShaderFile("shaders/standardMeshes.vtx.glsl");
	char* fragmentSource = ogl::loadShaderFile("shaders/standardMeshes.pix.glsl");
	// create shaders
	m_vShader = new ogl::VertexShader(vertexSource, "standardMeshes.vtx.glsl");
	m_fShader = new ogl::FragmentShader(fragmentSource, "standardMeshes.pix.glsl");
	// create program
	m_program.create(m_vShader, m_fShader);
  delete m_vShader;
  delete m_fShader;

	// set timer, hit log states and score
	m_timer = 45.0f;
	m_score = 0.0f;
	m_hitLog[0] = false;
	m_hitLog[1] = false;
	m_hitLog[2] = false;
	m_axeLifted[0] = false;
	m_axeLifted[1] = false;
	m_logAnimation = false;

	// set transform
	m_Axetransform.w.y = 3.0f;
	m_Axetransform.w.z = -3.5f;
	Matrix3 logOffset;
	logOffset.w.x = -0.179f;
	logOffset.w.y = 0.0f;
	m_mesh[1]->update(&logOffset);

	// load and start the music
	m_sound = new Sound("sounds/AxeGame.wav");
	m_sound->Start();
}
Axe::~Axe()
{
	// music must be stopped before being deleted
	m_sound->Stop();

  // cleanup memory
  delete m_texture[0];
	delete m_texture[1];
	delete m_texture[2];
	delete m_mesh[0];
	delete m_mesh[1];
	delete m_mesh[2];
	delete m_mesh[3];
	delete m_sound;
}
bool Axe::update(int input, float dt)
{
  // if player makes input update matrix
  switch(input)
	{
	case eW:
		// make sure player doesn't go off the screen
		if (m_Axetransform.w.y <= 3.25f)
		m_Axetransform.w.y += 4.0f * dt;
		break;
	case eS:
		// make sure player doesn't go off the screen
		if (m_Axetransform.w.y >= -5.0f)
			m_Axetransform.w.y -= 4.0f * dt;
		break;
  }
	
	// if the log has been hit three hits, start breaking animation
	if (m_logAnimation)
	{
		Matrix3 logMove = m_mesh[2]->matrix();
		Matrix3 logSmallMove = m_mesh[1]->matrix();
		logMove.w.x += -1.0f * dt;
		logMove.w.y += -4.0f * dt;
		logSmallMove.w.x += 1.0f * dt;
		logSmallMove.w.y += -4.0f * dt;
		m_mesh[2]->update(&logMove);
		m_mesh[1]->update(&logSmallMove);

		// done animation, now stop and reset the log
		if (m_mesh[1]->matrix().w.x > 2.5f)
		{
			m_logAnimation = false;
			logMove.w.x = 0.0f;
			logMove.w.y = 0.0f;
			logSmallMove.w.x = -0.179f;
			logSmallMove.w.y = 0.0f;
			m_mesh[2]->update(&logMove);
			m_mesh[1]->update(&logSmallMove);
		}
	}

	// testing to see if the player has traveled within the ring boundary
	if (m_Axetransform.w.y >= -2.0f && m_Axetransform.w.y <= -1.0f && !m_hitLog[0])
	{
			// increase score
  		m_score += 10.0f;

			// either hit the log once
			m_hitLog[0] = true;
	}
	else if (m_Axetransform.w.y >= -2.0f && m_Axetransform.w.y <= -1.0f && !m_hitLog[1] && m_axeLifted[0]) // log 2
	{
		// increase score
		m_score += 10.0f;

		// either hit the log two times
		m_hitLog[1] = true;
	}
	else if (m_Axetransform.w.y >= -2.0f && m_Axetransform.w.y <= -1.0f && !m_hitLog[2] && m_axeLifted[1]) // log 3
	{
		// increase score
		m_score += 30.0f;

		// either hit the log three times, broken the log
		m_hitLog[2] = true;

		m_logAnimation = true;
	}

	if (m_Axetransform.w.y >= 1.25f && m_hitLog[0])
	{
		m_axeLifted[0] = true;
	}
	if (m_Axetransform.w.y >= 1.25f && m_hitLog[1])
	{
		m_axeLifted[1] = true;
	}

	// if the log has been hit three times
	if (m_hitLog[0] && m_hitLog[1] && m_hitLog[2] && m_Axetransform.w.y >= 1.25f)
	{
		// reset state to false
		m_hitLog[0] = false;
		m_hitLog[1] = false;
		m_hitLog[2] = false;
		m_axeLifted[0] = false;
		m_axeLifted[1] = false;
	}

  // update mesh
  m_mesh[0]->update(&m_Axetransform);

	// decrease timer
	m_timer -= 1.0f * dt;
	std::cout << "m_score: " << m_score << std::endl;
	std::cout << "m_timer: " << m_timer << std::endl;

	// still got fuel the game carries on
	if (m_timer >= 0.1f)
	{
		return true;
	}
	else
	{
		// display score
		std::cout << "Your out of time, your score: " << m_score << std::endl;
		return false;
	}
}
bool Axe::update(float dt, vec2 leftHand, vec2& rightHand)
{
	bool bothHandsUsed = false;

	// takes user hand inputs
	if (rightHand.y <= 200.0f && leftHand.y <= 200.0f && m_Axetransform.w.y <= 3.25f)
	{
		m_Axetransform.w.y += 6.5f * dt;
		bothHandsUsed = true;
	}
	else if (rightHand.y <= 200.0f && m_Axetransform.w.y < 3.25f  || leftHand.y <= 200.0f && m_Axetransform.w.y < 3.25f)
	{
		m_Axetransform.w.y += 4.0f * dt;
	}
	if (rightHand.y >= m_height - 200.0f && leftHand.y >= m_height - 200.0f && m_Axetransform.w.y >= -5.0f)
	{
		m_Axetransform.w.y -= 6.5f * dt;
		bothHandsUsed = true;
	}
	else if (rightHand.y >= m_height - 200.0f && m_Axetransform.w.y >= -5.0f  || leftHand.y >= m_height - 200.0f && m_Axetransform.w.y >= -5.0f)
	{
		m_Axetransform.w.y -= 4.0f * dt;
	}

	// if the log has been hit three hits, start breaking animation
	if (m_logAnimation)
	{
		Matrix3 logMove = m_mesh[2]->matrix();
		Matrix3 logSmallMove = m_mesh[1]->matrix();
		logMove.w.x += -1.0f * dt;
		logMove.w.y += -4.0f * dt;
		logSmallMove.w.x += 1.0f * dt;
		logSmallMove.w.y += -4.0f * dt;
		m_mesh[2]->update(&logMove);
		m_mesh[1]->update(&logSmallMove);

		// done animation, now stop and reset the log
		if (m_mesh[1]->matrix().w.x > 2.5f)
		{
			m_logAnimation = false;
			logMove.w.x = 0.0f;
			logMove.w.y = 0.0f;
			logSmallMove.w.x = -0.179f;
			logSmallMove.w.y = 0.0f;
			m_mesh[2]->update(&logMove);
			m_mesh[1]->update(&logSmallMove);
		}
	}

	// testing to see if the player has traveled within the ring boundary
	if (m_Axetransform.w.y >= -2.0f && m_Axetransform.w.y <= -1.0f && !m_hitLog[0])
	{
		// were both hands used
		if (bothHandsUsed)
		{
			// double score
			m_score += 20.0f;
			bothHandsUsed = false;
		}
		else
		{
			// increase score
			m_score += 10.0f;
		}

		// either hit the log once
		m_hitLog[0] = true;
	}
	else if (m_Axetransform.w.y >= -2.0f && m_Axetransform.w.y <= -1.0f && !m_hitLog[1] && m_axeLifted[0]) // log 2
	{
		// were both hands used
		if (bothHandsUsed)
		{
			// double score
			m_score += 20.0f;
			bothHandsUsed = false;
		}
		else
		{
			// increase score
			m_score += 10.0f;
		}

		// either hit the log two times
		m_hitLog[1] = true;
	}
	else if (m_Axetransform.w.y >= -2.0f && m_Axetransform.w.y <= -1.0f && !m_hitLog[2] && m_axeLifted[1]) // log 3
	{
		// were both hands used
		if (bothHandsUsed)
		{
			// double score
			m_score += 60.0f;
			bothHandsUsed = false;
		}
		else
		{
			// increase score
			m_score += 30.0f;
		}

		// either hit the log three times, broken the log
		m_hitLog[2] = true;

		m_logAnimation = true;
	}

	if (m_Axetransform.w.y >= 1.25f && m_hitLog[0])
	{
		m_axeLifted[0] = true;
	}
	if (m_Axetransform.w.y >= 1.25f && m_hitLog[1])
	{
		m_axeLifted[1] = true;
	}

	// if the log has been hit three times
	if (m_hitLog[0] && m_hitLog[1] && m_hitLog[2] && m_Axetransform.w.y >= 1.25f)
	{
		// reset state to false
		m_hitLog[0] = false;
		m_hitLog[1] = false;
		m_hitLog[2] = false;
		m_axeLifted[0] = false;
		m_axeLifted[1] = false;
	}

	// update mesh
	m_mesh[0]->update(&m_Axetransform);

	// decrease timer
	m_timer -= 1.0f * dt;
	std::cout << "m_score: " << m_score << std::endl;
	//std::cout << "m_timer: " << m_timer << std::endl;

	// still got fuel the game carries on
	if (m_timer >= 0.1f)
	{
		return true;
	}
	else
	{
		// display score
		std::cout << "Your out of time, your score: " << m_score << std::endl;
		return false;
	}
}
void Axe::draw()
{
  	// camera transforms
	Matrix3 cameraTransform, projection, model, view, mv, mvp;

	// translate the model matrix
	model.w.x = m_Axetransform.w.x;
	model.w.y = m_Axetransform.w.y;
	model.w.z = m_Axetransform.w.z;

	// compute aspect ratio
	float aspectRatio = m_height / m_width;

	// compute the projection
	projection.perspective(90.0f * PI / 180, aspectRatio, 0.1f, 100.0f);
	// compute the View martix
	view = inverse(cameraTransform);

	// compute Model-View matrix
	mv = model * view;
	// make the matrix w.w value 1
	postiveW(mv);

	// compute Model-View-Projection matrix
	mvp.multMatrix(projection, mv);

  // bind texture
  m_texture[0]->bind(0);
	m_texture[1]->bind(1);
	m_texture[2]->bind(2);
  // bind program
  m_program.bind();

// Model-View-Projection martix elements copied into a float array
  float mvpArray[16] = {
			mvp.x.x, mvp.x.y, mvp.x.z, mvp.x.w, // X
			mvp.y.x, mvp.y.y, mvp.y.z, mvp.y.w, // Y
			mvp.z.x, mvp.z.y, mvp.z.z, mvp.z.w, // Z
			mvp.w.x, mvp.w.y, mvp.w.z, mvp.w.w, // W
		};

			// get uniform data for texture, this will be called 'ps_texture' within the shader
			glUniform1i(glGetUniformLocation(m_program.program(), "ps_texture"), 0);
			glUniform4f(glGetUniformLocation(m_program.program(), "ps_ambient"), 0.1f, 0.1f, 0.1f, 1.0f);
			glUniform4f(glGetUniformLocation(m_program.program(), "ps_diffuse"), 1.0f, 1.0f, 1.0f, 1.0f);
			glUniform4f(glGetUniformLocation(m_program.program(), "ps_specular"), 0.1f, 0.1f, 0.1f, 0.2f);
			glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mv"), 1, false, mvpArray);
			glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mvp"), 1, false, mvpArray);
  	// draw mesh
  	m_mesh[0]->draw(); 

		// get transform
		model.w.x = m_mesh[1]->matrix().w.x;
		model.w.y = m_mesh[1]->matrix().w.y;
		model.w.z = m_mesh[1]->matrix().w.z;
		// compute model-view
		mv = model * view;
		// compute Model-View-Projection matrix
		mvp.multMatrix(projection, mv);
		// update mvp
		mvpArray[0] = mvp.x.x;
		mvpArray[1] = mvp.x.y;
		mvpArray[2] = mvp.x.z;
		mvpArray[3] = mvp.x.w;
		mvpArray[4] = mvp.y.x;
		mvpArray[5] = mvp.y.y;
		mvpArray[6] = mvp.y.z;
		mvpArray[7] = mvp.y.w;
		mvpArray[8] = mvp.z.x;
		mvpArray[9] = mvp.z.y;
		mvpArray[10] = mvp.z.z;
		mvpArray[11] = mvp.z.w;
		mvpArray[12] = mvp.w.x;
		mvpArray[13] = mvp.w.y;
		mvpArray[14] = mvp.w.z;
		mvpArray[15] = mvp.w.w;

		// update uniforms
		glUniform1i(glGetUniformLocation(m_program.program(), "ps_texture"), 1);
		glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mvp"), 1, false, mvpArray);
		m_mesh[1]->draw();

		// get transform
		model.w.x = m_mesh[2]->matrix().w.x;
		model.w.y = m_mesh[2]->matrix().w.y;
		model.w.z = m_mesh[2]->matrix().w.z;
		// compute model-view
		mv = model * view;
		// compute Model-View-Projection matrix
		mvp.multMatrix(projection, mv);

		// update mvp
		mvpArray[0] = mvp.x.x;
		mvpArray[1] = mvp.x.y;
		mvpArray[2] = mvp.x.z;
		mvpArray[3] = mvp.x.w;
		mvpArray[4] = mvp.y.x;
		mvpArray[5] = mvp.y.y;
		mvpArray[6] = mvp.y.z;
		mvpArray[7] = mvp.y.w;
		mvpArray[8] = mvp.z.x;
		mvpArray[9] = mvp.z.y;
		mvpArray[10] = mvp.z.z;
		mvpArray[11] = mvp.z.w;
		mvpArray[12] = mvp.w.x;
		mvpArray[13] = mvp.w.y;
		mvpArray[14] = mvp.w.z;
		mvpArray[15] = mvp.w.w;

		glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mvp"), 1, false, mvpArray);
		m_mesh[2]->draw();

		// update the sky
		model.w.x = 0.0f;
		model.w.y = 0.0f;
		model.w.z = 0.0f;
		mv = model * view;
		// compute Model-View-Projection matrix
		mvp.multMatrix(projection, mv);
		// update mvp
		mvpArray[0] = mvp.x.x;
		mvpArray[1] = mvp.x.y;
		mvpArray[2] = mvp.x.z;
		mvpArray[3] = mvp.x.w;
		mvpArray[4] = mvp.y.x;
		mvpArray[5] = mvp.y.y;
		mvpArray[6] = mvp.y.z;
		mvpArray[7] = mvp.y.w;
		mvpArray[8] = mvp.z.x;
		mvpArray[9] = mvp.z.y;
		mvpArray[10] = mvp.z.z;
		mvpArray[11] = mvp.z.w;
		mvpArray[12] = mvp.w.x;
		mvpArray[13] = mvp.w.y;
		mvpArray[14] = mvp.w.z;
		mvpArray[15] = mvp.w.w;

		// update uniforms
		glUniform1i(glGetUniformLocation(m_program.program(), "ps_texture"), 2);
		glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mvp"), 1, false, mvpArray);
		m_mesh[3]->draw();

  // unbind program and texture
  m_program.unbind();
  m_texture[0]->unbind();
	m_texture[1]->unbind();
	m_texture[2]->unbind();
}