#include "mg_wood.h"

mg_wood::mg_wood(float height, float width)
{
  // create classes for minigames
  m_player = new Axe(height, width);
}
mg_wood::~mg_wood()
{
  // cleanup
  delete m_player;
}
bool mg_wood::update(int& EventCode, float& dt)
{
	// update classes
	return m_player->update(EventCode, dt);
}
bool mg_wood::update(float& dt, vec2& kinectLeftHand, vec2& kinectRightHand)
{
	// update classes
	return m_player->update(dt, kinectLeftHand, kinectRightHand);
}
void mg_wood::draw()
{
  // draw
  m_player->draw();
}
int mg_wood::gameScore()
{
	return m_player->score();
}
