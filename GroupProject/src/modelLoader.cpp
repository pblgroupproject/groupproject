// author : Antonio Almeida
// date   : 06/12/2014

#include "modelLoader.h"

ModelLoader::ModelLoader(const char* mesh)
{
	//initialising variables
	int size = 0;
	std::string content;

	// load the string into the buffer
	loadString(mesh, content, size);

	//Check for null
	if (content.empty())
	{
		std::cout << ".meshes file empty\n";
		return;
	}

	//intialise variables
	int numMesh;						//Number of Meshes
	char* nameMesh = new char[1024];	//Name of Mesh
	int numVertex;						//Number of Vertices
	int numIndices;						//Number of Indices

	//reading in the file
	std::istringstream iss(content);
	//read in: number of meshes, name of mesh, number of vertices, number of indices
	iss >> numMesh >> nameMesh >> numVertex >> numIndices;
	//We don't actually need the name of the mesh here
	delete[] nameMesh;

	// resize vectors
	m_meshData.resize(numVertex);
	m_meshIndices.resize(numIndices);

	float unrequiredData;
	// read in vertex, normal and uv data
	for (size_t i = 0; i < numVertex; ++i)
	{
		iss >> m_meshData[i].vertex.x >> m_meshData[i].vertex.y >> m_meshData[i].vertex.z // vertex data
			>> m_meshData[i].normal.x >> m_meshData[i].normal.y >> m_meshData[i].normal.z // normal data
			>> unrequiredData >> unrequiredData >> unrequiredData // tangent data (currently not needed)
			>> unrequiredData >> unrequiredData >> unrequiredData // bi-normal data (currently not needed)
			>> m_meshData[i].uv.x >> m_meshData[i].uv.y; // uv data
	}
	// read in indices
	for (size_t i = 0; i < numIndices; ++i)
	{
		iss >> m_meshIndices[i];
	}

	// gen vbo
	glGenBuffers(1, &m_vbo);
	// bind buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	// buffer data
	glBufferData(GL_ARRAY_BUFFER, sizeof(m_meshData)*m_meshData.size(), &m_meshData[0], GL_STATIC_DRAW);
	// unbind buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// gen vbo
	glGenBuffers(1, &m_ibo);
	// bind buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	// buffer data
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*m_meshIndices.size(), &m_meshIndices[0], GL_STATIC_DRAW);
	// unbind buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

ModelLoader::~ModelLoader()
{
	// clear what is in the vectors
	m_meshData.clear();
	m_meshIndices.clear();
	// delete buffers
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
}

void ModelLoader::loadString(const char* file, std::string &returnData, int &returnSize)
{
	//Create a string to read data into
	std::string data;
	//Choose the file you want to read in
	std::ifstream readFile (file);
	//If the file opened correctly, retrieve the data from it
	if (readFile.is_open())
	{
		//Whilst there is data copy it over
		while ( getline (readFile,data) )
		{
			//Store the data in our return string
			returnData.append(data);
			returnData.append("\n");
		}
		readFile.close();
	}
	else 
	{
		std::cout << "Unable to open file";
	}
}
void ModelLoader::update(Matrix3* matrix)
{
  m_matrix = *matrix;
}
void ModelLoader::draw()
{
		// bind vbo and ibo buffers
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
		// an offset from NULL
		const float* offset = 0;
		// enable vertex
		glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex_Normal_UV), offset);
		glEnableVertexAttribArray(0);
		// enable normals
		glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(Vertex_Normal_UV), offset + 4);
		glEnableVertexAttribArray(1);
		// enable (uv)
		glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(Vertex_Normal_UV), offset + 8);
		glEnableVertexAttribArray(2);

		// draw
		glDrawElements(GL_TRIANGLES, m_meshIndices.size(), GL_UNSIGNED_INT, 0);
		// disable from modification
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(0);
		// unbind buffers
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
}
