#include "Sound.h"
//  Code based on https://gist.github.com/armornick/3447121

/// Function to update .wav file position during playback
void my_audio_callback(void *userdata, Uint8 *stream, int len);

  Uint8 *audio_pos; ///< Global pointer to the audio buffer to be played
  Uint32 audio_len; ///< Remaining length of the sample we have to play 

Sound::Sound(const char* filename)
{
  //  Load the WAV
  if (SDL_LoadWAV(filename, &m_wav_sound, &m_wav_buffer, &m_wav_length) == NULL) 
  {
    fprintf(stderr, "Could not open sound.wav: %s\n", SDL_GetError());
  } 
  //  Assign callback function for playback
  m_wav_sound.callback = my_audio_callback;
  m_wav_sound.userdata = NULL;  
}


Sound::~Sound()
{
  //  Cleanup when finished
	SDL_FreeWAV(m_wav_buffer);
}

void Sound::Start()
{
 	audio_pos = m_wav_buffer; ///< copy sound buffer
  audio_len = m_wav_length; ///< copy file length 

  /* Open the audio device */
  if ( SDL_OpenAudio(&m_wav_sound, NULL) < 0 )
  {
    fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
   std::cin.get();
   std::cin.get();
    exit(-1);
  }
  /* Start playing */
  SDL_PauseAudio(0);
 
}

void Sound::Play()
{
  SDL_PauseAudio(0);
}

void Sound::Pause()
{
  SDL_PauseAudio(1);
}

void Sound::Stop()
{
  SDL_CloseAudio();
}

void my_audio_callback(void *userdata, Uint8 *stream, int len) 
{
  //  If .wav file has finished playing return
  if (audio_len ==0)
    return;

  //  update length
  len = ( len > audio_len ? audio_len : len );
  
  SDL_memcpy (stream, audio_pos, len); ///< simply copy from one buffer into the other
  SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);///< mix from one buffer into another
  //  Update position and time remaining
  audio_pos += len;
  audio_len -= len;
} 