#include "camera.h"

camera::camera(int w, int h)
{
	m_wHeight = h;
	m_wWidth = w;

	m_position = vec3(0.0f, 0.0f, 0.0f);
	m_direction =	vec3(0.0f, 0.0f, 1.0f);
	normalize(m_direction);
	m_up = vec3(0.0f, 1.0f, 0.0f);

	Initialize();
}

camera::camera(int w, int h, vec3 pos, vec3 dir, vec3 up)
{
	m_wHeight = w;
	m_wWidth =  h;

	m_position = pos;
	m_direction = normalize(dir);
	m_up = normalize(up);

	Initialize();
}

void camera::Initialize()
{
	vec3 HT = vec3(m_direction.x, 0.0f, m_direction.z);

	if (HT.z >= 0.0f)
    {
        if (HT.x >= 0.0f)
        {
            m_h = TWO_PI - asin(HT.z);
        }
        else
        {
            m_h = PI + asin(HT.z);
        }
    }
    else
    {
        if (HT.x >= 0.0f)
        {
            m_h = asin(-HT.z);
        }
        else
        {
            m_h = HALF_PI + asin(-HT.z);
        }
    }
    
    m_v = -asin(m_direction.y);
}

bool camera::keyboard()
{
	return false;
}

void camera::render()
{
}

void camera::Update()
{
	const vec3 VA(0.0f, 1.0f, 0.0f);

    // Vertical Rototation
    vec3 View = vec3(1.0f, 0.0f, 0.0f);
    //View.Rotate(m_h, VA);
	View = normalize(View);

    // Horizontal Rotation
    vec3 HA = cross(VA ,View);
    HA = normalize(HA);
    //View.Rotate(m_v, HA);
       
    m_direction = View;
    m_direction = normalize(m_direction);

    m_up = cross(HA, m_direction);
    m_up = normalize(m_up);
}
