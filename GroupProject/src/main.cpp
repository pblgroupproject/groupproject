#include "InputGUI.h"
#include "camera.h"
#include "Matrix.h"
#include "mg_Plane.h"
#include "mg_wood.h"

Window g_window;
bool g_gameLoop = true;
InputGUI g_IGUI;
DeltaTime g_dt;
Texture* g_tex[6];
Sound* g_sound;
ogl::Program g_program[2];
ogl::VertexShader* g_vShader;
ogl::FragmentShader* g_fShader;
kinectSensor g_kSensor; ///< XBOX KINECT sensor
mg_Plane* PlaneGame = NULL;
mg_wood*  WoodGame = NULL;

//----------------------------------------------------------------------------------------------------------------------
/// everything we want to initialise
//----------------------------------------------------------------------------------------------------------------------
void init()
{
	// init SDL
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		// SDL failed to init
		std::cout<<"SDL INIT FAILED\n";
		g_gameLoop = false;
	}

	// create our SDL window 
	g_window.createWindow();

	// set up OpenGL context with SDL
	initGLWindow(g_window);
	
	// setup GLEW
	initGLEW();

	// load shader source code
	char* vertexSource = ogl::loadShaderFile("shaders/menu.vtx.glsl");
	char* fragmentSource = ogl::loadShaderFile("shaders/menu.pix.glsl");
	// create shaders
	g_vShader = new ogl::VertexShader(vertexSource, "menu.vtx.glsl");
	g_fShader = new ogl::FragmentShader(fragmentSource, "menu.pix.glsl");
	// create program
	g_program[0].create(g_vShader, g_fShader);

	//Pointer to texture class
	g_tex[0] = new Texture();
	g_tex[1] = new Texture();
	g_tex[2] = new Texture();
	g_tex[3] = new Texture();
	g_tex[4] = new Texture();
	g_tex[5] = new Texture();
	//Send in image to the Texture Class
	g_tex[0]->LoadBMP("images/Game Main Menu.bmp");
	g_tex[1]->LoadBMP("images/Game Menu - Tetris.bmp");
	g_tex[2]->LoadBMP("images/Game Menu - Lumberjack .bmp");
	g_tex[3]->LoadBMP("images/Game Menu - Plane.bmp");
	g_tex[4]->LoadBMP("images/Instruction.bmp");
	g_tex[5]->LoadBMP("images/settings.bmp");

	// load up the sounds/music
	g_sound = new Sound("sounds/MainMenu.wav");

	// add a menu
	g_IGUI.addMenu(g_window.getSettings().m_userScreenSize);
	// add buttons for menu
	vec2 o(16.0f * g_window.ratioX(), 440.0f * g_window.ratioY());
	vec2 e(161.0f * g_window.ratioX(), 500.0f * g_window.ratioY());
	// add a button to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 186.0f * g_window.ratioX(); 
	o.y = 440.0f * g_window.ratioY();
	e.x = 336.0f * g_window.ratioX();
	e.y = 500.0f * g_window.ratioY();
	// add a buttons to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 357.0f * g_window.ratioX(); 
	o.y = 440.0f * g_window.ratioY();
	e.x = 504.0f * g_window.ratioX();
	e.y = 500.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 26.0f * g_window.ratioX(); 
	o.y = 394.0f * g_window.ratioY();
	e.x = 67.0f * g_window.ratioX();
	e.y = 434.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 438.0f * g_window.ratioX(); 
	o.y = 394.0f * g_window.ratioY();
	e.x = 478.0f * g_window.ratioX();
	e.y = 434.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);

	// add menu and set to active
	g_IGUI.addMenu(g_window.getSettings().m_userScreenSize);
	g_IGUI.activeMenu(1, g_window.getSettings().m_userScreenSize);
	// add buttons for menu
	o.x = 15.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 73.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	// add a button to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 186.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 336.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	// add a buttons to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 439.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 497.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 25.0f * g_window.ratioX(); 
	o.y = 396.0f * g_window.ratioY();
	e.x = 65.0f * g_window.ratioX();
	e.y = 438.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 447.0f * g_window.ratioX(); 
	o.y = 396.0f * g_window.ratioY();
	e.x = 487.0f * g_window.ratioX();
	e.y = 438.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);

	// add menu and set to active
	g_IGUI.addMenu(g_window.getSettings().m_userScreenSize);
	g_IGUI.activeMenu(2, g_window.getSettings().m_userScreenSize);
	// add buttons for menu
	o.x = 15.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 73.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	// add a button to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 186.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 336.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	// add a buttons to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 439.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 497.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 25.0f * g_window.ratioX(); 
	o.y = 396.0f * g_window.ratioY();
	e.x = 65.0f * g_window.ratioX();
	e.y = 438.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 447.0f * g_window.ratioX(); 
	o.y = 396.0f * g_window.ratioY();
	e.x = 487.0f * g_window.ratioX();
	e.y = 438.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);

	// add menu and set to active
	g_IGUI.addMenu(g_window.getSettings().m_userScreenSize);
	g_IGUI.activeMenu(3, g_window.getSettings().m_userScreenSize);
	// add buttons for menu
	o.x = 15.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 73.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	// add a button to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 186.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 336.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	// add a buttons to the menu
	g_IGUI.addMenuButton(o, e);
	o.x = 439.0f * g_window.ratioX(); 
	o.y = 444.0f * g_window.ratioY();
	e.x = 497.0f * g_window.ratioX();
	e.y = 503.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 25.0f * g_window.ratioX(); 
	o.y = 396.0f * g_window.ratioY();
	e.x = 65.0f * g_window.ratioX();
	e.y = 438.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 447.0f * g_window.ratioX(); 
	o.y = 396.0f * g_window.ratioY();
	e.x = 487.0f * g_window.ratioX();
	e.y = 438.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);

	// add menu and set to active
	g_IGUI.addMenu(g_window.getSettings().m_userScreenSize);
	g_IGUI.activeMenu(4, g_window.getSettings().m_userScreenSize);
	// add buttons for menu
	o.x = 236.0f * g_window.ratioX();
	o.y = 459.0f * g_window.ratioY();
	e.x = 276.0f * g_window.ratioX();
	e.y = 499.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);

	// add menu and set to active
	g_IGUI.addMenu(g_window.getSettings().m_userScreenSize);
	g_IGUI.activeMenu(5, g_window.getSettings().m_userScreenSize);
	// add buttons for menu
	o.x = 54.0f * g_window.ratioX();
	o.y = 145.0f * g_window.ratioY();
	e.x = 86.0f * g_window.ratioX();
	e.y = 171.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 54.0f * g_window.ratioX();
	o.y = 185.0f * g_window.ratioY();
	e.x = 86.0f * g_window.ratioX();
	e.y = 210.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 54.0f * g_window.ratioX();
	o.y = 224.0f * g_window.ratioY();
	e.x = 86.0f * g_window.ratioX();
	e.y = 250.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 54.0f * g_window.ratioX();
	o.y = 264.0f * g_window.ratioY();
	e.x = 86.0f * g_window.ratioX();
	e.y = 290.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 54.0f * g_window.ratioX();
	o.y = 304.0f * g_window.ratioY();
	e.x = 86.0f * g_window.ratioX();
	e.y = 330.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);
	o.x = 236.0f * g_window.ratioX();
	o.y = 459.0f * g_window.ratioY();
	e.x = 276.0f * g_window.ratioX();
	e.y = 499.0f * g_window.ratioY();
	g_IGUI.addMenuButton(o, e);

	// no menu added (i.e when in a game)
	g_IGUI.addMenu(g_window.getSettings().m_userScreenSize);

	// get the screen ratio
	vec2 screenSizeRatio;
	screenSizeRatio.x = g_window.ratioX();
	screenSizeRatio.y = g_window.ratioY();

	// turn on and setup up the Kinect, if successful then menu systems will accept Kinect input
	g_IGUI.kinectState(g_kSensor.sensorInit(screenSizeRatio));

	// set to main menu
	g_IGUI.activeMenu(0, g_window.getSettings().m_userScreenSize);

	g_sound->Start();
}
//----------------------------------------------------------------------------------------------------------------------
/// where we update
//----------------------------------------------------------------------------------------------------------------------
void update()
{
	// get delta time
	float dt = g_dt.update();

	// update GUI and events
	int updateEventCode = g_IGUI.update(g_gameLoop, dt);

	// get the players left and right hand and set it's position to screen co-ordinates
	vec2 playerRightHand = g_kSensor.rightHand();
	vec2 playerLeftHand = g_kSensor.leftHand();
	g_IGUI.setMouse(playerRightHand);
	
	switch (g_IGUI.currentMenu())
	{
		// main menu
	case 0:
		if (g_IGUI.lastMenuButtonPress() == 0)
			g_IGUI.activeMenu(1, g_window.getSettings().m_userScreenSize); // puzzle
		else if (g_IGUI.lastMenuButtonPress() == 1)
			g_IGUI.activeMenu(2, g_window.getSettings().m_userScreenSize); // lumberjack
		else if (g_IGUI.lastMenuButtonPress() == 2)
			g_IGUI.activeMenu(3, g_window.getSettings().m_userScreenSize); // plane
		else if (g_IGUI.lastMenuButtonPress() == 3)
			g_IGUI.activeMenu(5, g_window.getSettings().m_userScreenSize); // settings
		else if (g_IGUI.lastMenuButtonPress() == 4)
			g_IGUI.activeMenu(4, g_window.getSettings().m_userScreenSize); // instructions
		break;
		// puzzle menu
	case 1:
		if (g_IGUI.lastMenuButtonPress() == 1)
		{
			// game not completed so game will not load
			g_IGUI.activeMenu(1, g_window.getSettings().m_userScreenSize);
			//g_IGUI.activeMenu(6, g_window.getSettings().m_userScreenSize);
		}
		else if (g_IGUI.lastMenuButtonPress() == 4)
			g_IGUI.activeMenu(4, g_window.getSettings().m_userScreenSize); // instructions
		// last game (left arrow)
		else if (g_IGUI.lastMenuButtonPress() == 0)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(3, g_window.getSettings().m_userScreenSize);
		}
		// next game (right arrow)
		else if (g_IGUI.lastMenuButtonPress() == 2)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(2, g_window.getSettings().m_userScreenSize);
		}
		// home
		else if (g_IGUI.lastMenuButtonPress() == 3)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(0, g_window.getSettings().m_userScreenSize);
		}
		break;
		// lumberjack menu
	case 2:
		if (g_IGUI.lastMenuButtonPress() == 1)
		{
			// create game
			g_IGUI.activeMenu(6, g_window.getSettings().m_userScreenSize);
			if (WoodGame == NULL)
			{
				g_sound->Stop();
				WoodGame = new mg_wood(g_window.height(), g_window.width());
			}
		}
		else if (g_IGUI.lastMenuButtonPress() == 4)
			g_IGUI.activeMenu(4, g_window.getSettings().m_userScreenSize); // instructions
		// last game (left arrow)
		else if (g_IGUI.lastMenuButtonPress() == 0)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(1, g_window.getSettings().m_userScreenSize);
		}
		// next game (right arrow)
		else if (g_IGUI.lastMenuButtonPress() == 2)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(3, g_window.getSettings().m_userScreenSize);
		}
		// home
		else if (g_IGUI.lastMenuButtonPress() == 3)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(0, g_window.getSettings().m_userScreenSize);
		}
		break;
		// plane menu
	case 3:
		if (g_IGUI.lastMenuButtonPress() == 1)
		{
			g_IGUI.activeMenu(6, g_window.getSettings().m_userScreenSize);
			if (PlaneGame == NULL)
			{
				g_sound->Stop();
				PlaneGame = new mg_Plane(g_window.height(), g_window.width());
			}
		}
		else if (g_IGUI.lastMenuButtonPress() == 4)
			g_IGUI.activeMenu(4, g_window.getSettings().m_userScreenSize); // instructions
		// last game (left arrow)
		else if (g_IGUI.lastMenuButtonPress() == 0)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(2, g_window.getSettings().m_userScreenSize);
		}
		// next game (right arrow)
		else if (g_IGUI.lastMenuButtonPress() == 2)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(1, g_window.getSettings().m_userScreenSize);
		}
		// home
		else if (g_IGUI.lastMenuButtonPress() == 3)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(0, g_window.getSettings().m_userScreenSize);
		}
		break;
		// instructions menu
	case 4:
		if (g_IGUI.lastMenuButtonPress() == 0)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(0, g_window.getSettings().m_userScreenSize);
		}
		break;
		// settings menu
	case 5:
		// get the current settings, not to lose score data
		ProgramSettings screenSize = g_window.getSettings();

		if (g_IGUI.lastMenuButtonPress() == 5)
		{
			g_IGUI.resetLastButtonPressed();
			g_IGUI.activeMenu(0, g_window.getSettings().m_userScreenSize);
		}

		if (g_IGUI.lastMenuButtonPress() == 0)
		{
			g_IGUI.resetLastButtonPressed();

			// update settings
			screenSize.m_userScreenSize = 0;
			// apply update
			g_window.settings(screenSize);

			// print to console, there isn't currently a easy and quick way to draw a tick in the box.
			std::cout << "800x600 Windowed selected\n";
		}
		else if (g_IGUI.lastMenuButtonPress() == 1)
		{
			g_IGUI.resetLastButtonPressed();

			// update settings
			screenSize.m_userScreenSize = 1;
			// apply update
			g_window.settings(screenSize);

			// print to console, there isn't currently a easy and quick way to draw a tick in the box.
			std::cout << "1280x720 Windowed selected\n";
		}
		else if (g_IGUI.lastMenuButtonPress() == 2)
		{
			g_IGUI.resetLastButtonPressed();

			// update settings
			screenSize.m_userScreenSize = 2;
			// apply update
			g_window.settings(screenSize);

			// print to console, there isn't currently a easy and quick way to draw a tick in the box.
			std::cout << "1600x900 Windowed selected\n";
		}
		else if (g_IGUI.lastMenuButtonPress() == 3)
		{
			g_IGUI.resetLastButtonPressed();

			// update settings
			screenSize.m_userScreenSize = 3;
			// apply update
			g_window.settings(screenSize);

			// print to console, there isn't currently a easy and quick way to draw a tick in the box.
			std::cout << "1920x1080 Windowed selected\n";
		}
		else if (g_IGUI.lastMenuButtonPress() == 4)
		{
			g_IGUI.resetLastButtonPressed();

			// update settings
			screenSize.m_userScreenSize = 4;
			// apply update
			g_window.settings(screenSize);

			// print to console, there isn't currently a easy and quick way to draw a tick in the box.
			std::cout << "1920x1080 Fullscreen selected\n";
		}
		break;
	}

	// is the game being played
	bool planeInGame = false;
	bool woodInGame = false;

	// update mini game if Kinect being used
	if (PlaneGame && g_kSensor.state())
	{
		planeInGame = PlaneGame->update(dt, playerLeftHand, playerRightHand);
	}
	// updated mini game if no Kinect being used
	if (PlaneGame && !g_kSensor.state())
	{
		planeInGame = PlaneGame->update(updateEventCode, dt);
	}
	// if the game is not begin played and has been created, delete it
	if (planeInGame == false && PlaneGame)
	{
		// get the current settings
		ProgramSettings settings = g_window.getSettings();
		// if the score is a new high score save it to file
		if (PlaneGame->gameScore() > settings.m_PlaneScore)
		{
			std::cout << "NEW HIGH SCORE\n";
			settings.m_PlaneScore = PlaneGame->gameScore();
			g_window.settings(settings);
		}

		delete PlaneGame;
		PlaneGame = NULL;
		// reset the last button pressed and return player to Plane game menu
		g_IGUI.resetLastButtonPressed();
		g_IGUI.activeMenu(3, g_window.getSettings().m_userScreenSize);
		// restart the music
		g_sound->Start();
	}
	// update mini game if Kinect being used
	if (WoodGame && g_kSensor.state())
	{
		woodInGame = WoodGame->update(dt, playerLeftHand, playerRightHand);
	}
	//Update Mini Game
	if (WoodGame && !g_kSensor.state())
	{
		woodInGame = WoodGame->update(updateEventCode, dt);
	}
	// if the game is not begin played and has been created, delete it
	if (woodInGame == false && WoodGame)
	{
		// get the current settings
		ProgramSettings settings = g_window.getSettings();
		// if the score is a new high score save it to file
		if (WoodGame->gameScore() > settings.m_LumberjackScore)
		{
			std::cout << "NEW HIGH SCORE\n";
			settings.m_LumberjackScore = WoodGame->gameScore();
			g_window.settings(settings);
		}

		delete WoodGame;
		WoodGame = NULL;
		// reset the last button pressed and return player to Plane game menu
		g_IGUI.resetLastButtonPressed();
		g_IGUI.activeMenu(2, g_window.getSettings().m_userScreenSize);
		// restart the music
		g_sound->Start();
	}
}
//----------------------------------------------------------------------------------------------------------------------
/// draw to the window
//----------------------------------------------------------------------------------------------------------------------
void draw()
{
	// enable 2D textures in OpenGL
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	// clear the colour and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// process skeleton tracking
	g_kSensor.processSkeletonData();

	// if on a menu (not on noMenu)
	if(g_IGUI.currentMenu() < 6)
	{
		// bind program
		g_program[0].bind();
		// bind the texture of the menu we are currently on
		g_tex[g_IGUI.currentMenu()]->bind(0);
				// draw GUI
				g_IGUI.draw();
		// unbind program and texture
		g_tex[g_IGUI.currentMenu()]->unbind();
		g_program[0].unbind();

		// draw Kinect hand
		g_kSensor.draw();
    //Set game pointers to Null and delete if in mem
		if (PlaneGame)
		{
			delete PlaneGame;
			PlaneGame = NULL;
		}    
		if(WoodGame)
		    delete WoodGame;
		   WoodGame = NULL;
		}
	else // draw game
	{
    //Draw Plane Game
    if(PlaneGame)
    {
      PlaneGame->draw();
    }

		//Draw Wood Game
		if(WoodGame)
		 {
		   WoodGame->draw();
		 }
	}

	// swap the buffers (or in SDL's case window)
	SDL_GL_SwapWindow(g_window.window());
	
	// disable textures and depth testing
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
}


//----------------------------------------------------------------------------------------------------------------------
/// main - dont use, put code in functions above
//----------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	init();
	// gameloop where all updates and draw calls are made
	while(g_gameLoop)
	{
		update();
		draw();
	}
	// ----------- cleanup and delete anything that you have created ----------- //
  if(PlaneGame)
    delete PlaneGame;
	g_sound->Stop();
	g_vShader->~VertexShader();
	g_fShader->~FragmentShader();
	delete g_tex[0];
	delete g_tex[1];
	delete g_tex[2];
	delete g_tex[3];
	delete g_tex[4];
	delete g_tex[5];
	delete g_sound;
	g_IGUI.~InputGUI();
	g_window.~Window();
	g_kSensor.~kinectSensor();
	SDL_Quit();

	return 0;
}
