// author : Lewis Ward
// date   : 22/10/2014

#include "Xkinect.h"

kinectSensor::kinectSensor()
{
	// no sensor has been detected
	m_sensorDetected = false;
}
kinectSensor::~kinectSensor()
{
	// make sure we have a Kinect sensor to actually shutdown
	if(m_sensorDetected)
	{
		// shut down the Kinect
		m_sensor->NuiShutdown();
	}
}
bool kinectSensor::sensorInit(vec2 screenRatio)
{
	// get the screen ratio
	m_screenRatio = screenRatio;

	// we need to pass a int pointer into the 'NuiGetSensorCount' function
	int kinectCount = 1;
	// get the number of senors connected, returns S_OK if successful otherwise failed
	HRESULT sensorCheck =  NuiGetSensorCount(&kinectCount);
	if(FAILED(sensorCheck))
	{
		std::cout<<"no Kinect connected\n";
		return m_sensorDetected;
	}
	else
	{
		// create instance of the Kinect sensor, as we only got one sensor we don't need to worry about the index to much
		// the index can range from zero to one-minus the total amount of sensors (defined by NuiGetSensorCount)
		sensorCheck = NuiCreateSensorByIndex(0, &m_sensor);
		if(FAILED(sensorCheck))
		{
			// could create the index
			std::cout<<"couldn't create sensor index\n";
			return m_sensorDetected;
		}
		else
		{
			// if the status is ok initialize the Kinect sensor
			// using depth data with player index, skeleton data and colour data
			sensorCheck = m_sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_SKELETON | NUI_INITIALIZE_FLAG_USES_COLOR | NUI_INITIALIZE_FLAG_USES_DEPTH);
			// if initialize was susseccful
			if(SUCCEEDED(sensorCheck))
			{
				// create event for skeleton
				m_skeletonTrack = CreateEventW(NULL, TRUE, FALSE, NULL);
				// need to have two image streams open at the same time, one for the RGB camera and the other for the depth camera
				// open the image stream for the RGB camera
				m_sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480, 0, 2, NULL, &m_rgbCamera);
				// open the image stream for the depth camera
				m_sensor->NuiImageStreamOpen(NUI_IMAGE_TYPE_DEPTH, NUI_IMAGE_RESOLUTION_640x480, 0, 2, NULL, &m_depthCamera);
				// enable skeleton tracking
				sensorCheck = m_sensor->NuiSkeletonTrackingEnable(m_skeletonTrack, NUI_SKELETON_TRACKING_FLAG_ENABLE_SEATED_SUPPORT);
				// was skeleton tracking enabled
				if(FAILED(sensorCheck))
				{
					std::cout<<"Failed to enable skeleton tracking\n";
				}
			}
		}
	}
	m_sensorDetected = true;
}
void kinectSensor::draw()
{
	// less then ideal way of going about this, but it works
	glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(m_rightHand.x, m_rightHand.y, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(m_rightHand.x - 25.0f, m_rightHand.y + 25.0f, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(m_rightHand.x + 25.0f, m_rightHand.y + 25.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(m_leftHand.x, m_leftHand.y, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(m_leftHand.x - 25.0f, m_leftHand.y + 25.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(m_leftHand.x + 25.0f, m_leftHand.y + 25.0f, 0.0f);
	glEnd();
}
void kinectSensor::nextFrameData(GLubyte* nextFrame)
{
	// make sure we have a Kinect sensor connected
	if(m_sensorDetected)
	{
		// struct contains colour or depth data of a frame
		NUI_IMAGE_FRAME currentFrame;
		// if we get the get frame from Kinect image stream
		HRESULT nextFrameOk = m_sensor->NuiImageStreamGetNextFrame(m_rgbCamera, 0, &currentFrame);
		// did we get the next frame
    if(FAILED(nextFrameOk))
		{
			// failed to get next frame
			return;
		}
		// frame image date is equal to the current frames' data 
		INuiFrameTexture* frameTexture = currentFrame.pFrameTexture;
		// a locked rectangle surface
		NUI_LOCKED_RECT ractangleSur;
		// buffer is locked so we can have read and write access
		frameTexture->LockRect(0, &ractangleSur, NULL, 0);
		// if there is data in a row (number of bytes in a row)
		if(ractangleSur.Pitch != 0)
		{
			// pointer to the upper left corner of the ractangle
			const BYTE* curr = (const BYTE*)ractangleSur.pBits;
			// the end of our data
		  const BYTE* dataEnd = curr + (KIN_WIDTH * KIN_HEIGHT) * 4;
			// write data to our next frame
			while(curr < dataEnd) 
			{
				*nextFrame++ = *curr++;
		  }
		}
		// unlock the buffer
		frameTexture->UnlockRect(0);
		// release the frame
		m_sensor->NuiImageStreamReleaseFrame(m_rgbCamera, &currentFrame);
	}
}
void kinectSensor::nextDepthFrameData(GLubyte* nextFrame)
{
	// make sure we have a Kinect sensor connected
	if(m_sensorDetected)
	{
		// struct contains colour or depth data of a frame
		NUI_IMAGE_FRAME currentFrame;
		// if we get the get frame from Kinect image stream
		HRESULT nextFrameOk = m_sensor->NuiImageStreamGetNextFrame(m_depthCamera, 0, &currentFrame);
		// did we get the next frame
    if(FAILED(nextFrameOk))
		{
			// failed to get next frame
			return;
		}
    // frame image date is equal to the current frames' data 
		INuiFrameTexture* frameTexture = currentFrame.pFrameTexture;
		// a locked rectangle surface
		NUI_LOCKED_RECT ractangleSur;
		// buffer is locked so we can have read and write access
		frameTexture->LockRect(0, &ractangleSur, NULL, 0);
		// if there is data in a row (number of bytes in a row)
		if(ractangleSur.Pitch != 0)
    {
			// the Kinect will store the pixels distance (depth) from the kinect to the object within the pixel itself, the
			// distance is in millimeters. So this means that each pixel in the depth image stores the depth information
			// and player information (which player is assoiated with that pixel). The depth data is  stored in 16 bits.
			// 16 bit pointer to pBits pixel (upper left corner)
			const uint16_t* curr = (const uint16_t*)ractangleSur.pBits;
			// 16 bit pointer to the last pixel in our frame
			const uint16_t* dataEnd = curr + (KIN_WIDTH * KIN_HEIGHT);
			// cycle from first to last pixel in our frame
			while(curr < dataEnd) 
			{
			  // gets the depth of the pixel
				uint16_t depth = NuiDepthPixelToDepth(*curr++);
				// remember that our image data is RGBA, to make it a greyscale image we need to set the RGB depth % 256
				for(int i = 0; i < 3; ++i)
				{
					*nextFrame++ = (BYTE)depth % 256;
				}
				// set the alpha to 1
				*nextFrame++ = 0xff;
			}
    }
    // unlock the buffer
		frameTexture->UnlockRect(0);
		// release the frame
		m_sensor->NuiImageStreamReleaseFrame(m_depthCamera, &currentFrame);
	}
}
void kinectSensor::processSkeletonData()
{
	// make sure we have a Kinect sensor connected
	if(m_sensorDetected)
	{
		// wait for 0ms to update skeleton 
		if(WAIT_OBJECT_0 == WaitForSingleObject(m_skeletonTrack, 0))
    {
			// create skeleton frame
			NUI_SKELETON_FRAME skeletonFrame = {0};
			
			// Get the skeleton frame that is ready
			if (SUCCEEDED(m_sensor->NuiSkeletonGetNextFrame(0, &skeletonFrame)))
			{
				nextFrameReady(&skeletonFrame);
			}
    }
	}
}
void kinectSensor::drawSkeleton(const NUI_SKELETON_DATA& skeleton)
{
	// chest/torso bones
  drawBone(skeleton, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_LEFT);
  drawBone(skeleton, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_RIGHT);
  drawBone(skeleton, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SPINE);
  drawBone(skeleton, NUI_SKELETON_POSITION_SPINE, NUI_SKELETON_POSITION_HIP_CENTER);
  drawBone(skeleton, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_LEFT);
  drawBone(skeleton, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_RIGHT);
	
	// arms
  drawBone(skeleton, NUI_SKELETON_POSITION_SHOULDER_LEFT, NUI_SKELETON_POSITION_ELBOW_LEFT);
  drawBone(skeleton, NUI_SKELETON_POSITION_ELBOW_LEFT, NUI_SKELETON_POSITION_WRIST_LEFT);
  drawBone(skeleton, NUI_SKELETON_POSITION_WRIST_LEFT, NUI_SKELETON_POSITION_HAND_LEFT);
  drawBone(skeleton, NUI_SKELETON_POSITION_SHOULDER_RIGHT, NUI_SKELETON_POSITION_ELBOW_RIGHT);
  drawBone(skeleton, NUI_SKELETON_POSITION_ELBOW_RIGHT, NUI_SKELETON_POSITION_WRIST_RIGHT);
  drawBone(skeleton, NUI_SKELETON_POSITION_WRIST_RIGHT, NUI_SKELETON_POSITION_HAND_RIGHT);
}
void kinectSensor::drawBone(const NUI_SKELETON_DATA& skeleton, NUI_SKELETON_POSITION_INDEX jOne, NUI_SKELETON_POSITION_INDEX jTwo)
{
	// what is the state of the joints
	NUI_SKELETON_POSITION_TRACKING_STATE jOneState = skeleton.eSkeletonPositionTrackingState[jOne];
  NUI_SKELETON_POSITION_TRACKING_STATE jTwoState = skeleton.eSkeletonPositionTrackingState[jTwo];
	
	// the joints have not been tracked so can't draw
  if(jOneState == NUI_SKELETON_POSITION_NOT_TRACKED || jTwoState == NUI_SKELETON_POSITION_NOT_TRACKED )
  {
		return;
  }

	// get joints position 
	const Vector4& jOnePos = skeleton.SkeletonPositions[jOne];
  const Vector4& jTwoPos = skeleton.SkeletonPositions[jTwo];

	// get the left and right hand joints
	if (NUI_SKELETON_POSITION_HAND_LEFT == jTwo)
	{
		m_leftHand = toScreen(jTwoPos);
	}
	if (NUI_SKELETON_POSITION_HAND_RIGHT == jTwo)
	{
		m_rightHand = toScreen(jTwoPos);
	}

	// don't draw if both points are inferred (placeholder in position array or untracked joint - i.e person sitting down
	// so camera cannot detect that joint!)
	// ------------- UNCOMMENT IF STATEMENTS TO DRAW ALL TRACKED BONES (FOR DEBUGGING) ---------------//
  //if(jOneState == NUI_SKELETON_POSITION_INFERRED || jTwoState == NUI_SKELETON_POSITION_INFERRED)
  //{
	//	// draw a line between joints
  //  drawBonesLines(jOnePos, jTwoPos);
  //}
  //// both joints are being tracked (by default all joint assumed to be inferred)
  //if(jOneState == NUI_SKELETON_POSITION_TRACKED && jTwoState == NUI_SKELETON_POSITION_TRACKED)
  //{
	//	// draw a line between joints
  //  drawBonesLines(jOnePos, jTwoPos);
  //}
}
vec2 kinectSensor::toScreen(Vector4 jointLocation)
{
	LONG x, y;
	USHORT depth;
	// use a transform to convert skeleton to a depth image
	NuiTransformSkeletonToDepthImage(jointLocation, &x, &y, &depth, NUI_IMAGE_RESOLUTION_640x480);
	// return the screen space vector
	vec2 temp((float)x * m_screenRatio.x, (float)y * m_screenRatio.y);
	return temp;
}
void kinectSensor::nextFrameReady(NUI_SKELETON_FRAME* sFrame)
{
	for(int i = 0; i < NUI_SKELETON_COUNT; i++)
	{
		// cycle every joint and test the skeleton data
		const NUI_SKELETON_DATA& sData = sFrame->SkeletonData[i];

		// is the joint being tracked or not
		switch (sData.eTrackingState)
		{
		case NUI_SKELETON_TRACKED:
					drawSkeleton(sData);
		   break;
		case NUI_SKELETON_POSITION_ONLY:
					drawPosition(sData.Position);
		    break;
		default:
				break;
		}
	}
}
void kinectSensor::drawPosition(Vector4 position)
{
	// convert vec4 to vec2
	vec2 a;
	a = toScreen(position);
	// draw point
	glBegin(GL_POINTS);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex2f(a.x, a.y);
		glColor3f(1.0f, 1.0f, 1.0f);
	glEnd();
}
void kinectSensor::drawBonesLines(Vector4 jointA, Vector4 jointB)
{
	// convert vec4 to vec2
	vec2 a, b;
	a = toScreen(jointA);
	b = toScreen(jointB);
	// draw lines
	glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(a.x, a.y);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex2f(b.x, b.y);
		glColor3f(1.0f, 1.0f, 1.0f);
	glEnd();
}
