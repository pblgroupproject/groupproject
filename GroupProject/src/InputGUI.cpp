// author : Lewis Ward
// date   : 21/11/2014
#include "InputGUI.h"

MenuButton::MenuButton(vec2 origin, vec2 end)
{
	// set member variables
	m_origin = origin;
	m_end = end;
}
bool MenuButton::clickedSelected(vec2& mPos)
{
	// if mouse button was pressed within boundy.
	if(mPos.x >= m_origin.x && mPos.y >= m_origin.y && mPos.x <= m_end.x && mPos.y <= m_end.y)
	{
		// button clicked
		return true;
	}
	// button wasn't clicked
	return false;
}
Menu::Menu(vec2 screenSize)
{
	// set connected state to false and counter to zero
	m_kinectConnected = false;
	m_buttonTimeCounter = 0;

	m_listenCode.push_back(eMBDownL);

	// set the last button pressed to 100, we will never have 100 buttons on one menu
	m_lastButtonPressed = 100;
	// set screen size
	m_screenSize.x = screenSize.x;
	m_screenSize.y = screenSize.y;

	// vertex array
	Vertex_UV menuVertices[4];
	// set vertex data
	menuVertices[0].vertex.x = -1.0f;
	menuVertices[0].vertex.y = 1.0f;
	menuVertices[0].vertex.z = 0.0f;
	menuVertices[0].uv.x = 0.0f;
	menuVertices[0].uv.y = 0.0f;
	menuVertices[1].vertex.x = 1.0f;
	menuVertices[1].vertex.y = 1.0f;
	menuVertices[1].vertex.z = 0.0f;
	menuVertices[1].uv.x = 1.0f;
	menuVertices[1].uv.y = 0.0f;
	menuVertices[2].vertex.x = -1.0f;
	menuVertices[2].vertex.y = -1.0f;
	menuVertices[2].vertex.z = 0.0f;
	menuVertices[2].uv.x = 0.0f;
	menuVertices[2].uv.y = 1.0f;
	menuVertices[3].vertex.x = 1.0f;
	menuVertices[3].vertex.y = -1.0f;
	menuVertices[3].vertex.z = 0.0f;
	menuVertices[3].uv.x = 1.0f;
	menuVertices[3].uv.y = 1.0f;
	// indices
	int index[] =
	{
		0, 1, 2,
		2, 3, 1 
	};

	// genereate a buffer
	glGenBuffers(1, &m_vbo);
	// bind
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex_UV)*4, &menuVertices[0], GL_STATIC_DRAW);
	// unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// genereate a buffer
	glGenBuffers(1, &m_ibo);
	// bind
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*6, &index[0], GL_STATIC_DRAW);
	// unbind
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
Menu::~Menu()
{
	// check if texture has been deleted, if so set 'm_menuBackground' to NULL/delete
	// clear the vector of buttons
	m_buttons.clear();
}
void Menu::draw()
{
	// bind vbo and ibo buffers
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	// an offset from NULL
	const float* offset = 0;
	// enable vertex
	glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex_UV), offset);
	glEnableVertexAttribArray(0);
	// enable (uv)
	glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(Vertex_UV), offset + 4);
	glEnableVertexAttribArray(1);
	// draw
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	// disable from modification
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
	// unbind buffers
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void Menu::addButton(vec2 o, vec2 e)
{
	// add a button to the array
	m_buttons.push_back(MenuButton(o, e));
}
void Menu::removeButton()
{
	// make sure there is at least a button in the array
	if(m_buttons.size())
	{
		// remove a button to the array
		m_buttons.pop_back();
	}
}
void Menu::listen(int& code, vec2 updateMouse, float dt)
{
	// if Kinect connected and detected
	if (m_kinectConnected)
	{
		// cycle all event codes in vector
		for (size_t i = 0; i < m_buttons.size(); ++i)
		{
			bool buttonClicked = m_buttons[i].clickedSelected(m_kinectMouse);
			if (buttonClicked)
			{
				// update counter by one
				m_buttonTimeCounter += 1.0f * dt;
			}
			if (m_buttonTimeCounter >= 3)
			{
				// update last button pressed
				m_lastButtonPressed = i;
				// reset counter
				m_buttonTimeCounter = 0.0f;
			}
		}
	}
	// if the left mouse button has been pressed
	if(eMBDownL == code)
	{
		// cycle all event codes in vector
		for(size_t i = 0; i < m_buttons.size(); ++i)
		{
			bool buttonClicked = m_buttons[i].clickedSelected(updateMouse);
			if(buttonClicked)
			{
				// update last button pressed
				m_lastButtonPressed = i;
			}
		}
	}
}
InputGUI::InputGUI()
{
	m_currentMenu = 0; // set the current active menu 
}
InputGUI::~InputGUI()
{
}
int InputGUI::update(bool& gameloop, float dt)
{
	// update event handler
	int updateCode = m_GUIEventHandler.update();
	// has user asked to quit program
	if(updateCode == 1)
	{
		gameloop = false; // user quit program
	}

	// InputGUI will listen for events, if it gets a message from an event it's listening for it will trigger an action
	m_menus[m_currentMenu].listen(updateCode, m_GUIEventHandler.mousePosition(), dt);

	// return update code
	return updateCode;
}
void InputGUI::draw()
{
	// draw active menu
	m_menus[m_currentMenu].draw();
}
void InputGUI::activeMenu(int index, vec2 screenSize)
{
	// set active
	m_currentMenu = index;
	// set screen size
	m_menus[m_currentMenu].menuSize(screenSize);
}
void InputGUI::addMenu(vec2 size)
{
	// add new menu
	m_menus.push_back(size);
}
void InputGUI::addMenuButton(vec2 origin, vec2 end)
{
	m_menus[m_currentMenu].addButton(origin, end);
}
void InputGUI::removeMenuButton()
{
	// remove menu
	m_menus.pop_back();
}
int InputGUI::lastMenuButtonPress()
{
	return m_menus[m_currentMenu].getButtonPressed();
}
void InputGUI::resetLastButtonPressed()
{
	for (size_t i = 0; i < m_menus.size(); ++i)
	{
		m_menus[i].resetButtonPressed();
	}
}
void InputGUI::kinectState(bool state)
{
	for (size_t i = 0; i < m_menus.size(); ++i)
	{
		m_menus[i].connectedKinect(state);
	}
}
