// author : Lewis Ward
// date   : 21/11/2014
#include "EventHandler.h"

EHandler::EHandler()
{

}
EHandler::~EHandler()
{

}
int EHandler::update()
{
	while(SDL_PollEvent(&m_events))
	{
		switch(m_events.type)
		{
		case SDL_QUIT:
				return eQuit; // user clicked on the close button
			break;
		case SDL_KEYDOWN:
			// if the Esc key has been pressed quit the program
			if(SDLK_ESCAPE == m_events.key.keysym.sym)
			{ return eQuit; }
			// W/A/S/D, space and arrow keys
			if (SDLK_w == m_events.key.keysym.sym)
			{ return eW; }
			if (SDLK_a == m_events.key.keysym.sym)
			{ return eA; }
			if (SDLK_s == m_events.key.keysym.sym)
			{ return eS; }
			if (SDLK_d == m_events.key.keysym.sym)
			{ return eD; }
			if (SDLK_SPACE == m_events.key.keysym.sym)
			{ return eSpace; }
			if (SDLK_UP == m_events.key.keysym.sym)
			{ return eUp; }
			if (SDLK_LEFT == m_events.key.keysym.sym)
			{ return eLeft; }
			if (SDLK_DOWN == m_events.key.keysym.sym)
			{ return eDown; }
			if (SDLK_RIGHT == m_events.key.keysym.sym)
			{ return eRight; }
			// if number keys are pressed
			if(SDLK_0 == m_events.key.keysym.sym)
			{ return e0; }
			if(SDLK_1 == m_events.key.keysym.sym)
			{ return e1; }
			if(SDLK_2 == m_events.key.keysym.sym)
			{ return e2; }
			if(SDLK_3 == m_events.key.keysym.sym)
			{ return e3; }
			if(SDLK_4 == m_events.key.keysym.sym)
			{ return e4; }
			if(SDLK_5 == m_events.key.keysym.sym)
			{ return e5; }
			if(SDLK_6 == m_events.key.keysym.sym)
			{ return e6; }
			if(SDLK_7 == m_events.key.keysym.sym)
			{ return e7; }
			if(SDLK_8 == m_events.key.keysym.sym)
			{ return e8; }
			if(SDLK_9 == m_events.key.keysym.sym)
			{ return e9; }
			// if the left and right alt or crtl key is pressed
			if(SDLK_LALT == m_events.key.keysym.sym)
			{ return eLA; }
			if(SDLK_LCTRL == m_events.key.keysym.sym)
			{ return eLC; }
			if(SDLK_RALT == m_events.key.keysym.sym)
			{ return eRA; }
			if(SDLK_RCTRL == m_events.key.keysym.sym)
			{ return eRC; }
			break;
				 // mouse buttons pressed
		case SDL_MOUSEBUTTONDOWN:
			  if(SDL_BUTTON_LEFT == m_events.button.button)
				{ return eMBDownL; }
				if(SDL_BUTTON_MIDDLE == m_events.button.button)
				{ return eMBDownM; }
				if(SDL_BUTTON_RIGHT == m_events.button.button)
				{ return eMBDownR; }
			break;

		case SDL_MOUSEMOTION:
			{
				// update mouse position
				m_mouseCoords.x = m_events.motion.x;
				m_mouseCoords.y = m_events.motion.y;
				return eMMotion;
			}
			break;

		case SDL_MOUSEBUTTONUP:
				// mouse buttons released
				if(SDL_BUTTON_LEFT)
				{
					m_mouseCoords.x = m_events.motion.x;
					m_mouseCoords.y = m_events.motion.y;
					return eMBUpL;
				}
				if(SDL_BUTTON_MIDDLE)
				{
					m_mouseCoords.x = m_events.motion.x;
					m_mouseCoords.y = m_events.motion.y;
					return eMBUpM;
				}
				if(SDL_BUTTON_RIGHT)
				{
					m_mouseCoords.x = m_events.motion.x;
					m_mouseCoords.y = m_events.motion.y;
					return eMBUpR;
				}
			break;
		// default enum 
		default: return eNoEvent;
			break;
		}
	}
}

DeltaTime::DeltaTime()
{
	// gets the current performance counter frequency, performance counter frequency value is fixed when the system
	// boots up so is consistent across processors.
	QueryPerformanceFrequency((LARGE_INTEGER*)&m_startFrame);
	// compute the counts per second
	m_secondsCount = 1.0f / m_startFrame;
	// get value for last frame
	QueryPerformanceCounter((LARGE_INTEGER*)&m_lastFrame);
}
DeltaTime::~DeltaTime()
{

}
float DeltaTime::update()
{
	// get the current performance counter frequency for the new frame
	QueryPerformanceCounter((LARGE_INTEGER*)&m_newFrame);
	// compute the delta time 
	float dt = (m_newFrame - m_lastFrame) * m_secondsCount;
	// set last time value with new time 
	m_lastFrame = m_newFrame;
	// return dt
	return dt;
}
