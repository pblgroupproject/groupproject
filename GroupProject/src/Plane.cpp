#include "Plane.h"

Plane::Plane(float height, float width)
{
  // setting member variables
  m_height = height;
  m_width = width;
  m_texture[0] = new Texture;
	m_texture[1] = new Texture;
	m_texture[2] = new Texture;
	m_texture[0]->LoadBMP("images/Plane.bmp");
	m_texture[1]->LoadBMP("images/planeRing.bmp");
	m_texture[2]->LoadBMP("images/PlaneSky.bmp");
  m_mesh[0] = new ModelLoader("meshes/Plane.meshes");
	m_mesh[1] = new ModelLoader("meshes/planeRing.meshes");
	m_mesh[2] = new ModelLoader("meshes/skyPlane.meshes");

  // load shader source code
	char* vertexSource = ogl::loadShaderFile("shaders/standardMeshes.vtx.glsl");
	char* fragmentSource = ogl::loadShaderFile("shaders/standardMeshes.pix.glsl");
	// create shaders
	m_vShader = new ogl::VertexShader(vertexSource, "standardMeshes.vtx.glsl");
	m_fShader = new ogl::FragmentShader(fragmentSource, "standardMeshes.pix.glsl");
	// create program
	m_program.create(m_vShader, m_fShader);
  delete m_vShader;
  delete m_fShader;


	// set starting matrix values
	m_Planetransform.w.z = -5.0f;
	Matrix3 ringsStarting; 
	ringsStarting.w.x = 0.0f;
	ringsStarting.w.y = 0.0f;
	ringsStarting.w.z = -50.0f;
	m_mesh[1]->update(&ringsStarting);

	// set to zero
	m_score = 0;
	m_hitRing[0] = false;
	m_hitRing[1] = false;
	m_hitRing[2] = false;

	m_fuel = 70.0f;

	// load and start the music
	m_sound = new Sound("sounds/PlaneGame.wav");
	m_sound->Start();
}
Plane::~Plane()
{
	// music must be stopped before being deleted
	m_sound->Stop();

  // cleanup memory
  delete m_texture[0];
	delete m_texture[1];
	delete m_texture[2];
  delete m_mesh[0];
	delete m_mesh[1];
	delete m_mesh[2];
	delete m_sound;
}
bool Plane::update(int input, float dt)
{
  // if player makes input update matrix
  switch(input)
	{
	case eA:
		// make sure player doesn't go off the screen
		if (m_Planetransform.w.x >= -5.0f)
		m_Planetransform.w.x -= 2.5f * dt;
		break;
	case eD:
		// make sure player doesn't go off the screen
		if (m_Planetransform.w.x <= 5.0f)
		m_Planetransform.w.x += 2.5f * dt;
		break;
  }

	// update rings matrix
	Matrix3 rings = m_mesh[1]->matrix();
	rings.w.z += 3.25f * dt;

	// send rings back to starting position
	if (rings.w.z > 26.0f)
	{
		rings.w.z = -50.0f;
		// reset state to false
		m_hitRing[0] = false;
		m_hitRing[1] = false;
		m_hitRing[2] = false;
	}

	// testing to see if the player has traveled within the ring boundary
	if (rings.w.z >= -5.2f && rings.w.z <= -4.9f) // ring 1
	{
		if (m_mesh[0]->matrix().w.x >= -3.25f && m_mesh[0]->matrix().w.x <= 3.25f && m_hitRing[0] == false)
		{
			// increase fuel
			m_fuel += 8.0f;
		}
		// either hit the ring or missed it
		m_hitRing[0] = true;
	}
	else if (rings.w.z >= -5.0f + 12.4f && rings.w.z <= -5.0f + 12.52f) // ring 2
	{
		if (m_mesh[0]->matrix().w.x >= 5.5f + -3.25f && m_mesh[0]->matrix().w.x <= 5.5f + 3.25f && m_hitRing[1] == false)
		{
			// increase fuel
			m_fuel += 8.0f;
		}
		// either hit the ring or missed it
		m_hitRing[1] = true;
	}
	else if (rings.w.z >= -5.0f + 25.0f && rings.w.z <= -5.0f + 25.2f) // ring 3
	{
		if (m_hitRing[2] == false && m_mesh[0]->matrix().w.x >= -3.25f + -3.5f && m_mesh[0]->matrix().w.x <= 3.25f + -3.5f)
		{
			// increase fuel
			m_fuel += 8.0f;
		}
		// either hit the ring or missed it
		m_hitRing[2] = true;
	}

	// update mesh
	m_mesh[0]->update(&m_Planetransform);
	m_mesh[1]->update(&rings);

	// increase score and decrease fuel
	m_score++;
	m_fuel -= 1.5f * dt;
	std::cout << "m_score: " << m_score << std::endl;
	std::cout << "m_fuel : " << m_fuel << std::endl;

	// still got fuel the game carries on
	if (m_fuel >= 0.1f)
	{
		return true;
	}
	else
	{
		std::cout << "Your out of fuel, your score: " << m_score << std::endl;
		return false;
	}
}
bool Plane::update(float dt, vec2 leftHand, vec2& rightHand)
{
	// takes user hand inputs and makes sure player doesn't go off the screen
	if (leftHand.x < 100.0f && m_Planetransform.w.x >= -5.0f)
	{
		m_Planetransform.w.x -= 2.5f * dt;
	}
	if (rightHand.x > m_width - 100.0f && m_Planetransform.w.x <= 5.0f)
	{
		m_Planetransform.w.x += 2.5f * dt;
	}
	
	// update rings matrix
	Matrix3 rings = m_mesh[1]->matrix();
	rings.w.z += 3.25f * dt;

	// send rings back to starting position
	if (rings.w.z > 26.0f)
	{
		rings.w.z = -50.0f;
		// reset state to false
		m_hitRing[0] = false;
		m_hitRing[1] = false;
		m_hitRing[2] = false;
	}

	// testing to see if the player has traveled within the ring boundary
	if (rings.w.z >= -5.2f && rings.w.z <= -4.9f) // ring 1
	{
		if (m_mesh[0]->matrix().w.x >= -3.25f && m_mesh[0]->matrix().w.x <= 3.25f && m_hitRing[0] == false)
		{
			// increase fuel
			m_fuel += 8.0f;
		}
		// either hit the ring or missed it
		m_hitRing[0] = true;
	}
	else if (rings.w.z >= -5.0f + 12.4f && rings.w.z <= -5.0f + 12.52f) // ring 2
	{
		if (m_mesh[0]->matrix().w.x >= 5.5f + -3.25f && m_mesh[0]->matrix().w.x <= 5.5f + 3.25f && m_hitRing[1] == false)
		{
			// increase fuel
			m_fuel += 8.0f;
		}
		// either hit the ring or missed it
		m_hitRing[1] = true;
	}
	else if (rings.w.z >= -5.0f + 25.0f && rings.w.z <= -5.0f + 25.2f) // ring 3
	{
		if (m_hitRing[2] == false && m_mesh[0]->matrix().w.x >= -3.25f + -3.5f && m_mesh[0]->matrix().w.x <= 3.25f + -3.5f)
		{
			// increase fuel
			m_fuel += 8.0f;
		}
		// either hit the ring or missed it
		m_hitRing[2] = true;
	}

	// update mesh
	m_mesh[0]->update(&m_Planetransform);
	m_mesh[1]->update(&rings);

	// increase score and decrease fuel
	m_score++;
	m_fuel -= 1.5f * dt;
	std::cout << "m_score: " << m_score << std::endl;
	std::cout << "m_fuel : " << m_fuel << std::endl;

	// still got fuel the game carries on
	if (m_fuel >= 0.1f)
	{
		return true;
	}
	else
	{
		// display score
		std::cout << "Your out of fuel, your score: " << m_score << std::endl;
		return false;
	}
}
void Plane::draw()
{
  // camera transforms
	Matrix3 cameraTransform, projection, model, view, mv, mvp;

	// translate the model matrix
	model.w.x = m_Planetransform.w.x;
	model.w.y = m_Planetransform.w.y;
  model.w.z = m_Planetransform.w.z;

	cameraTransform.w.y = 1.5f;

	// compute aspect ratio
	float aspectRatio = m_height / m_width;

	// compute the projection
	projection.perspective(90.0f * PI / 180, aspectRatio, 0.1f, 50.0f);
	// compute the View martix
	view = inverse(cameraTransform);

	// compute Model-View matrix
	mv = model * view;
	// make the matrix w.w value 1
	postiveW(mv);

	// compute Model-View-Projection matrix
	mvp.multMatrix(projection, mv);

  // bind texture
  m_texture[0]->bind(0);
	m_texture[1]->bind(1);
	m_texture[2]->bind(2);

  // bind program
  m_program.bind();
	// Model-View-Projection martix elements copied into a float array
  float mvpArray[16] = {
			mvp.x.x, mvp.x.y, mvp.x.z, mvp.x.w, // X
			mvp.y.x, mvp.y.y, mvp.y.z, mvp.y.w, // Y
			mvp.z.x, mvp.z.y, mvp.z.z, mvp.z.w, // Z
			mvp.w.x, mvp.w.y, mvp.w.z, mvp.w.w, // W
		};

		// get uniform data for texture, this will be called 'ps_texture' within the shader
		glUniform1i(glGetUniformLocation(m_program.program(), "ps_texture"), 0);
		glUniform4f(glGetUniformLocation(m_program.program(), "ps_ambient"), 0.2f, 0.2f, 0.2f, 1.0f);
		glUniform4f(glGetUniformLocation(m_program.program(), "ps_diffuse"), 0.9f, 0.9f, 0.9f, 1.0f);
		glUniform4f(glGetUniformLocation(m_program.program(), "ps_specular"), 0.0f, 0.0f, 0.0f, 1.0f);
		glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mv"), 1, false, mvpArray);
		glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mvp"), 1, false, mvpArray);
  	// draw mesh
  	m_mesh[0]->draw(); 

		// update the rings
		model.w.x = 0.0f;
		model.w.y = 0.0f;
		model.w.z = m_mesh[1]->matrix().w.z;
		mv = model * view;
		// compute Model-View-Projection matrix
		mvp.multMatrix(projection, mv);

		// update mvp
		mvpArray[0] = mvp.x.x;
		mvpArray[1] = mvp.x.y;
		mvpArray[2] = mvp.x.z;
		mvpArray[3] = mvp.x.w;
		mvpArray[4] = mvp.y.x;
		mvpArray[5] = mvp.y.y;
		mvpArray[6] = mvp.y.z;
		mvpArray[7] = mvp.y.w;
		mvpArray[8] = mvp.z.x;
		mvpArray[9] = mvp.z.y;
		mvpArray[10] = mvp.z.z;
		mvpArray[11] = mvp.z.w;
		mvpArray[12] = mvp.w.x;
		mvpArray[13] = mvp.w.y;
		mvpArray[14] = mvp.w.z;
		mvpArray[15] = mvp.w.w;

		// update uniforms
		glUniform1i(glGetUniformLocation(m_program.program(), "ps_texture"), 1);
		glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mvp"), 1, false, mvpArray);
		m_mesh[1]->draw();

		// update the sky
		model.w.z = 0.0f;
		mv = model * view;
		// compute Model-View-Projection matrix
		mvp.multMatrix(projection, mv);
		// update mvp
		mvpArray[0] = mvp.x.x;
		mvpArray[1] = mvp.x.y;
		mvpArray[2] = mvp.x.z;
		mvpArray[3] = mvp.x.w;
		mvpArray[4] = mvp.y.x;
		mvpArray[5] = mvp.y.y;
		mvpArray[6] = mvp.y.z;
		mvpArray[7] = mvp.y.w;
		mvpArray[8] = mvp.z.x;
		mvpArray[9] = mvp.z.y;
		mvpArray[10] = mvp.z.z;
		mvpArray[11] = mvp.z.w;
		mvpArray[12] = mvp.w.x;
		mvpArray[13] = mvp.w.y;
		mvpArray[14] = mvp.w.z;
		mvpArray[15] = mvp.w.w;

		// update uniforms
		glUniform1i(glGetUniformLocation(m_program.program(), "ps_texture"), 2);
		glUniformMatrix4fv(glGetUniformLocation(m_program.program(), "vs_mvp"), 1, false, mvpArray);
		m_mesh[2]->draw();

  // unbind program and texture
  m_program.unbind();
  m_texture[0]->unbind();
	m_texture[1]->unbind();
	m_texture[2]->unbind();
}
 