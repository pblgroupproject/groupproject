// author : Lewis Ward
// date   : 15/10/2014

#include "Window.h"

bool initGLEW()
{
	// turn on experimental
	glewExperimental = GL_TRUE;
	// init glew
	GLenum error = glewInit();
	// make sure there wasn't any errors
	if(GLEW_OK != error)
	{
		std::cout<<"Error: GLEW failed\n";
		return false;
	}
	return true;
}

Window::Window()
{
	// set window X and Y positions
	m_xPos = 0;
	m_yPos = 30;

	// set the defalut screen
	m_width = 800;
	m_height = 600;
	m_resX = 800.0f / 512.0f;
	m_resY = 600.0f / 512.0f;

	loadProgramSettings(m_settings);
}
Window::~Window()
{
	SDL_GL_DeleteContext(m_context);
	// destory windown
	SDL_DestroyWindow(m_win);
	// save settings
	saveProgramSettings(m_settings);
}
void Window::createWindow()
{
  // create window in certain mode from settings file
	switch(m_settings.m_userScreenSize)
	{
	case 0:
		// create window
		m_win = SDL_CreateWindow("Kinsi", m_xPos, m_yPos, m_width, m_height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
		m_resX = (float)m_width / 512.0f;
		m_resY = (float)m_height / 512.0f;
		break;
	case 1: 		
		m_win = SDL_CreateWindow("Kinsi", m_xPos, m_yPos, 1280, 720, SDL_WINDOW_SHOWN  | SDL_WINDOW_OPENGL);
		// get the size of the window
		SDL_GetWindowSize(m_win, &m_width, &m_height);
		// compute the difference between the texture size and window size (so bounding boxes are relative to screen size)
		m_resX = (float)m_width / 512.0f;
		m_resY = (float)m_height / 512.0f;
		break;
	case 2: 		
		m_win = SDL_CreateWindow("Kinsi", m_xPos, m_yPos, 1600, 900, SDL_WINDOW_SHOWN  | SDL_WINDOW_OPENGL);
		// get the size of the window
		SDL_GetWindowSize(m_win, &m_width, &m_height);
		// compute the difference between the texture size and window size (so bounding boxes are relative to screen size)
		m_resX = (float)m_width / 512.0f;
		m_resY = (float)m_height / 512.0f;
		break;
	case 3: 		
		m_win = SDL_CreateWindow("Kinsi", m_xPos, m_yPos, 1920, 1080, SDL_WINDOW_SHOWN  | SDL_WINDOW_OPENGL);
		// get the size of the window
		SDL_GetWindowSize(m_win, &m_width, &m_height);
		// compute the difference between the texture size and window size (so bounding boxes are relative to screen size)
		m_resX = (float)m_width / 512.0f;
		m_resY = (float)m_height / 512.0f;
		break;
	case 4:
		// create window
		m_win = SDL_CreateWindow("Kinsi", m_xPos, m_yPos, m_width, m_height,
			                       SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL);
		// get the size of the screen
		SDL_Rect screenBoundBox;
		SDL_GetDisplayBounds(0, &screenBoundBox);
		m_width = screenBoundBox.w;
		m_height = screenBoundBox.h;
		// compute the difference between the texture size and window size (so bounding boxes are relative to screen size)
		m_resX = (float)m_width / 512.0f;
		m_resY = (float)m_height / 512.0f;
		break;
	default:
		// create window
		m_win = SDL_CreateWindow("Kinsi", m_xPos, m_yPos, m_width, m_height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
		m_resX = (float)m_width / 512.0f;
		m_resY = (float)m_height / 512.0f;
		break;
	}
}

void initGLWindow(Window& win)
{
	// make a OpenGL Context from the window
	win.GLConext(SDL_GL_CreateContext(win.window()));
	SDL_GL_MakeCurrent(win.window(), win.getGLConext());
	// creating the viewport
	glViewport(0, 0, win.width(), win.height());
	// setup projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// multiply matrix with orthographic matrix
	glOrtho(0, win.width(), win.height(), 0, 0, 50);
	// modelview matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	// clear screen to set colour
	glClearColor(0.0f, 1.0f, 1.0f, 0.0f);
}

bool loadProgramSettings(ProgramSettings& fileSettings)
{
	// input file stream for settings
	std::ifstream settingsFile("settings.txt");
	// if the file was opened
	if(settingsFile.is_open())
	{
		// load in data
		settingsFile >> fileSettings.m_userScreenSize;
		settingsFile >> fileSettings.m_PlaneScore;
		settingsFile >> fileSettings.m_LumberjackScore;
		settingsFile >> fileSettings.m_TetrisScore;
		// file loaded
		return true;
	}
	else
	{
		// close the file and display message
		settingsFile.close();
		std::cout<<"Load Failed : Program Settings\n";
		// set setting in case it failed
		fileSettings.m_userScreenSize = 0;
		fileSettings.m_PlaneScore = 0;
		fileSettings.m_LumberjackScore = 0;
		fileSettings.m_TetrisScore = 0;
		// file failed
		return false;
	}
}
bool saveProgramSettings(ProgramSettings& fileSettings)
{
	// output file stream for settings
	std::ofstream outFile("settings.txt");
	// if the file was opened
	if(outFile.is_open())
	{
		// save the data to file
		outFile << fileSettings.m_userScreenSize << std::endl;
		outFile << fileSettings.m_PlaneScore << std::endl;;
		outFile << fileSettings.m_LumberjackScore << std::endl;;
		outFile << fileSettings.m_TetrisScore << std::endl;;
		// file saved
		return true;
	}
	else
	{
		// close the file and display message
		outFile.close();
		std::cout<<"Save Failed : Program Settings\n";
		// file failed to save
		return false;
	}
}
