#include "Window.h"
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Texture of a .bmp image file
//----------------------------------------------------------------------------------------------------------------------
class Texture
{
private:

	SDL_Surface* m_surf;	///< Surface to convert from
	uint32_t m_textures;	///< for texture
	int32_t m_colourNum;	///< the colour number
	GLenum m_texFormat;		///< the format which the texture is in
	int m_width;					///< height of the image
	int m_height;					///< width of the image

public:
	/// \breif Constructor
	Texture();
	/// \breif Destructor
	~Texture();

	/// \breif Loads a BMP image 
	/// \prama const char* filename
	void LoadBMP(std::string filename);
	/// \breif bind a texture
	/// \prama int texture slot
	void bind(int slot);
	/// \breif unbinds the texture
	void unbind();
	/// \breif get the image width
	/// \prama int width
	inline int width()
	{ return m_width; }
	/// \breif get the image height
	/// \prama int height
	inline int height()
	{ return m_height; }
	/// \breif returns the texture 
	/// \prama uint32_t
	inline uint32_t ReturnTexture()
	{ return m_textures; }
};
