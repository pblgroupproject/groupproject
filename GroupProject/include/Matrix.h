#pragma once
#include "Vectors.h"
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Matrix2 struct
//----------------------------------------------------------------------------------------------------------------------
struct Matrix2
{
  vec2 x; ///< x component
  vec2 y; ///< y component
  vec2 w; ///< position of transform

  
  Matrix2() : x(1.0f, 0.0f), y(0.0f, 1.0f), w(0.0f, 0.0f) {}//< constructor functions (initialises to the Identity matrix)
  
  /// initialises the matrix from 6 floats 
  Matrix2(float xx, float xy, 
          float yx, float yy,
          float wx, float wy) : x(xx, xy), y(yx, yy), w(wx, wy)
  {}
  
  /// initialises the matrix from 3x2D vectors
  Matrix2(vec2 X, 
          vec2 Y,
          vec2 W) : x(X), y(Y), w(W)
  {}
  
  friend vec2 rotate(const Matrix2& m, vec2 v);
  friend vec2 tranform(const Matrix2& m, vec2 v);

  /// translate in local space
  void moveLocal(vec2 offset)
    {
      offset = rotate(*this, offset);
      w += offset;
    }

  /// translate in world coordinate
  void moveGlobal(vec2 offset)
    {
      w += offset;
    }
};

/// rotate 2D vector
inline vec2 rotate(const Matrix2& m, vec2 v)
{
  vec2 r = m.x * v.x;
  r += m.y * v.y;
  return r;
}

/// transform 2D point by matrix 
inline vec2 transform(const Matrix2& m, vec2 p)
{
  return rotate(m, p) + m.w;
}

/// rotate 2D vector by inverse of matrix 
inline vec2 inverseRotate(const Matrix2& m, vec2 v)
{
  vec2 r;
  r.x = dot(v, m.x) / dot(m.x, m.x);
  r.y = dot(v, m.y) / dot(m.y, m.y);
  return r;
}

/// transform 2D point by the inverse of a matrix 
inline vec2 inverseTransform(const Matrix2& m, vec2 p)
{
  return inverseRotate(m, p - m.w);
}

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Matrix3 struct
//----------------------------------------------------------------------------------------------------------------------
struct Matrix3
{
  vec3 x; ///< x compnent
  vec3 y; ///< y compnent
  vec3 z; ///< z compnent
  vec3 w; ///< position of the transform

  /// constructor functions (initialises to the Identity matrix)
  Matrix3() 
    : x(1.0f, 0.0f, 0.0f), 
      y(0.0f, 1.0f, 0.0f), 
      z(0.0f, 0.0f, 1.0f), 
      w(0.0f, 0.0f, 0.0f) {}

  /// Constructs a 3D matrix from a 2D one
  Matrix3(const Matrix2& m) 
    : x(m.x), 
      y(m.y), 
      z(0.0f, 0.0f, 1.0f), 
      w(m.w) {}
  
  /// initialises the matrix from 12 floats 
  Matrix3(float xx, float xy, float xz, 
          float yx, float yy, float yz,
          float zx, float zy, float zz,
          float wx, float wy, float wz)
     : x(xx, xy, xz), y(yx, yy, yz), z(zx, zy, zz), w(wx, wy, wz)
  {}
  
  /// initialises the matrix from 4x3D vectors
  Matrix3(vec3 X, 
          vec3 Y,
          vec3 Z,
          vec3 W) : x(X), y(Y), z(Z), w(W)
  {}
  
  /// sets this matrix to a X rotation matrix
  void setRotateX(float rotate)
  {
    float ca = std::cosf(rotate);
    float sa = std::sinf(rotate);
    x = vec3(1.0f, 0.0f, 0.0f);
    y = vec3(0.0f, ca, sa);
    z = vec3(0.0f,-sa, ca);
    w = vec3();
  }
  
  /// sets this matrix to a Y rotation matrix
  void setRotateY(float rotate)
  {
    float ca = std::cosf(rotate);
    float sa = std::sinf(rotate);
    x = vec3(ca, 0.0f, -sa);
    y = vec3(0.0f, 1.0f, 0.0f);
    z = vec3(sa, 0.0f, ca);
    w = vec3();
  }
  
  /// sets this matrix to a Z rotation matrix
  void setRotateZ(float rotate)
  {
    float ca = std::cosf(rotate);
    float sa = std::sinf(rotate);
    x = vec3(ca, sa, 0.0f);
    y = vec3(-sa, ca, 0.0f);
    z = vec3(0.0f, 0.0f, 1.0f);
    w = vec3();
  }
  
  friend vec3 rotate(const Matrix3& m, vec3 v);
  friend vec3 tranform(const Matrix3& m, vec3 v);

  /// translate transform relative to local coordinate frame
  void moveLocal(vec3 offset)
    {
      offset = rotate(*this, offset);
      w += offset;
    }

  /// translate transform relative to world coordinate frame
  void moveGlobal(vec3 offset)
    {
      w += offset;
    }

	// will compute the projection matrix for a right handed frustum
	/// \prama float left co-ordinate
	/// \prama float right co-ordinate
	/// \prama float bottom co-ordinate
	/// \prama float top co-ordinate
	/// \prama float near viewing plane
	/// \prama float far viewing plane
	void frustum(float l, float r, float b, float t, float nearP, float farP)
	{
		// OpenGL is column major
		//  X   Y   Z    W
		// m^1 m^5 m^9  m^13
		// m^2 m^6 m^10 m^14
		// m^3 m^7 m^11 m^15
		// m^4 m^8 m^12 m^16

		// some useful vectors
		vec3 oneX(1.0f, 0.0f, 0.0f), oneY(0.0f, 1.0f, 0.0f), oneZ(0.0f, 0.0f, 1.0f);
		// X column
		x = oneX * (2.0f * nearP / (r - l)); ///< right (r) - left (l) = direction
		// Y column
		y = oneY * (2.0f * nearP / (t - b)); ///< top (t) - bottom (b) = height
		// Z column
		z = vec3((r + l) / (r - l), (t + b) / (t - b), (farP + nearP) / (nearP - farP));
		// set the w component of Z
		z.w = -1.0f;
		// W column
		w = oneZ * (2.0f * farP * nearP / (nearP - farP));
	}

	// will compute the projection matrix for a right handed frustum
	/// \prama float field of view angle (radians)
	/// \prama float aspect ratio of window
	/// \prama float near viewing plane
	/// \prama float far viewing plane
	void perspective(float angle, float aRatio, float nearPlane, float farPlane)
	{
		// compute near plane rectangual boundary (i.e width and height)
		const float scale = std::tanf(angle / 2.0f) * nearPlane;
		const float right = aRatio * scale;
		const float left = -right; ///< negate right
		const float top = scale;
		const float bottom = -top; ///< negate top
		// compute the frustum
		frustum(left, right, bottom, top, nearPlane, farPlane);
	}

	// multiply two 4x4 matrix together, this will transform the child martix to gobal co-ordinates.
	/// \prama Mat3 parent matrix
	/// \prama Mat3 child martix
	void multMatrix(const Matrix3& parent, const Matrix3& child)
	{
		x.x = parent.x.x * child.x.x;
		x.y = parent.x.y * child.x.x;
		x.z = parent.x.z * child.x.x;
		x.w = parent.x.w * child.x.x;
		x.x += parent.y.x * child.x.y;
		x.y += parent.y.y * child.x.y;
		x.z += parent.y.z * child.x.y;
		x.w += parent.y.w * child.x.y;
		x.x += parent.z.x * child.x.z;
		x.y += parent.z.y * child.x.z;
		x.z += parent.z.z * child.x.z;
		x.w += parent.z.w * child.x.z;
		x.x += parent.w.x * child.x.w;
		x.y += parent.w.y * child.x.w;
		x.z += parent.w.z * child.x.w;
		x.w += parent.w.w * child.x.w;
		y.x = parent.x.x * child.y.x;
		y.y = parent.x.y * child.y.x;
		y.z = parent.x.z * child.y.x;
		y.w = parent.x.w * child.y.x;
		y.x += parent.y.x * child.y.y;
		y.y += parent.y.y * child.y.y;
		y.z += parent.y.z * child.y.y;
		y.w += parent.y.w * child.y.y;
		y.x += parent.z.x * child.y.z;
		y.y += parent.z.y * child.y.z;
		y.z += parent.z.z * child.y.z;
		y.w += parent.z.w * child.y.z;
		y.x += parent.w.x * child.y.w;
		y.y += parent.w.y * child.y.w;
		y.z += parent.w.z * child.y.w;
		y.w += parent.w.w * child.y.w;
		z.x = parent.x.x * child.z.x;
		z.y = parent.x.y * child.z.x;
		z.z = parent.x.z * child.z.x;
		z.w = parent.x.w * child.z.x;
		z.x += parent.y.x * child.z.y;
		z.y += parent.y.y * child.z.y;
		z.z += parent.y.z * child.z.y;
		z.w += parent.y.w * child.z.y;
		z.x += parent.z.x * child.z.z;
		z.y += parent.z.y * child.z.z;
		z.z += parent.z.z * child.z.z;
		z.w += parent.z.w * child.z.z;
		z.x += parent.w.x * child.z.w;
		z.y += parent.w.y * child.z.w;
		z.z += parent.w.z * child.z.w;
		z.w += parent.w.w * child.z.w;
		w.x = parent.x.x * child.w.x;
		w.y = parent.x.y * child.w.x;
		w.z = parent.x.z * child.w.x;
		w.w = parent.x.w * child.w.x;
		w.x += parent.y.x * child.w.y;
		w.y += parent.y.y * child.w.y;
		w.z += parent.y.z * child.w.y;
		w.w += parent.y.w * child.w.y;
		w.x += parent.z.x * child.w.z;
		w.y += parent.z.y * child.w.z;
		w.z += parent.z.z * child.w.z;
		w.w += parent.z.w * child.w.z;
		w.x += parent.w.x * child.w.w;
		w.y += parent.w.y * child.w.w;
		w.z += parent.w.z * child.w.w;
		w.w += parent.w.w * child.w.w;
	}
};

/// rotate 3D vector by matrix 
inline vec3 rotate(const Matrix3& m, vec3 v)
{
  vec3 r = m.x * v.x;
  r += m.y * v.y;
  r += m.z * v.z;
  return r;
}

/// transform 3D point by matrix 
inline vec3 transform(const Matrix3& m, vec3 p)
{
  return rotate(m, p) + m.w;
}

/// rotate 3D vector by the inverse of a matrix 
inline vec3 inverseRotate(const Matrix3& m, vec3 v)
{
  vec3 r;
  r.x = dot(v, m.x) / dot(m.x, m.x);
  r.y = dot(v, m.y) / dot(m.y, m.y);
  r.z = dot(v, m.z) / dot(m.z, m.z);
  return r;
}

/// transform 3D point by the inverse of a matrix 
inline vec3 inverseTransform(const Matrix3& m, vec3 p)
{
  return inverseRotate(m, p - m.w);
}

/// invert matrix
inline Matrix3 inverse(const Matrix3& m)
{
	const float x2 = 1.0f / dot(m.x, m.x);
	const float y2 = 1.0f / dot(m.y, m.y);
	const float z2 = 1.0f / dot(m.z, m.z);
	Matrix3 mi;
	mi.x.x = m.x.x * x2;
	mi.x.y = m.y.x * y2;
	mi.x.z = m.z.x * z2;
	mi.y.x = m.x.y * x2;
	mi.y.y = m.y.y * y2;
	mi.y.z = m.z.y * z2;
	mi.z.x = m.x.z * x2;
	mi.z.y = m.y.z * y2;
	mi.z.z = m.z.z * z2;
	mi.w = -rotate(mi, m.w);
	return mi;	
}

/// inverts the matrix m, and returns the inverted matrix.
/// This method is faster, but will fail if there is scaling in matrix m
inline Matrix3 fastInverse(const Matrix3& m)
{
	Matrix3 mi;
	mi.x.x = m.x.x;
	mi.x.y = m.y.x;
	mi.x.z = m.z.x;
	mi.y.x = m.x.y;
	mi.y.y = m.y.y;
	mi.y.z = m.z.y;
	mi.z.x = m.x.z;
	mi.z.y = m.y.z;
	mi.z.z = m.z.z;
	mi.w = -rotate(mi, m.w);
	return mi;
}

/// multiplies the local space child matrix by its parent, and returns the child matrix in world space 
inline Matrix3 operator * (const Matrix3& child, const Matrix3& parent)
{
  Matrix3 r;
  r.x = rotate(parent, child.x);
  r.y = rotate(parent, child.y);
  r.z = rotate(parent, child.z);
  r.w = transform(parent, child.w);
  return r;
}

inline void postiveW(Matrix3& matrix)
{ matrix.w.w = 1; }

inline void negativeW(Matrix3& matrix)
{ matrix.w.w = 0; }
