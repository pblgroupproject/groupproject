// author : Lewis Ward
// date   : 15/10/2014

#pragma once
#define GLEW_STATIC
#include <iostream>
#include <fstream>
#include <string>
#include <stdint.h>
#include "gl/glew.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include "Vectors.h"

#define PI 3.14159265359f	///< Float Pi
#define TWO_PI 6.28318530718f	///< Float 2 Pi
#define HALF_PI 1.57079632679f	///< Float Half Pi

enum screenSize
{
	s800x600Windowed,
	s1280x720Windowed,
	s1600x900Windowed,
	s1920x1080Windowed,
	s1920x1080Fullscreen,
};
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Initialise GLEW
//----------------------------------------------------------------------------------------------------------------------
extern bool initGLEW();

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Settings file structure
//----------------------------------------------------------------------------------------------------------------------
struct ProgramSettings
{
	int m_userScreenSize; // screen size the user has defined
	int m_PlaneScore; // high score for plane game
	int m_LumberjackScore; // high score for lumber jack game
	int m_TetrisScore; // high score for puzzle game
};

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Window class for creating a SDL window
//----------------------------------------------------------------------------------------------------------------------
class Window
{
public:
	/// \brief Constr
	Window();
	/// \brief Destr
	~Window();

	/// \brief create the window
	void createWindow();
	/// \brief get the SDL_Window pointer for this window
	/// \return SDL_Window*
	inline SDL_Window* window()
	{ return m_win; }
	/// \brief get the width of the window
	/// \return int
	inline int width()
	{ return m_width; }
	/// \brief get the height of the window
	/// \return int
	inline int height()
	{ return m_height; }
	/// \brief get the scalar for screen X ratio
	/// \return int
	inline float ratioX()
	{ return m_resX; }
	/// \brief get the scalar for screen Y ratio
	/// \return int
	inline float ratioY()
	{ return m_resY; }
	/// \brief set the program settings
	/// \prama ProgramSettings
	inline void settings(ProgramSettings& set)
	{ m_settings = set; }
	/// \brief get the program settings
	/// \return ProgramSettings
	inline ProgramSettings getSettings()
	{ return m_settings; }
	/// \brief set the GL context
	/// \prama SDL_GLContext
	inline void GLConext(SDL_GLContext context)
	{ m_context = context; }
	/// \brief get the GL context
	/// \return SDL_GLContext
	inline SDL_GLContext getGLConext()
	{ return m_context; }

private:
	SDL_Window* m_win; 
	int m_xPos;
	int m_yPos;
	int m_width;
	int m_height;
	float m_resX;
	float m_resY;
	ProgramSettings m_settings;
	SDL_GLContext m_context;
};

/// \brief  Creates the OpenGL context
/// \prama  Window
void initGLWindow(Window& win);
/// \brief  Loads the program settings from file
/// \return  bool true if successful
bool loadProgramSettings(ProgramSettings& fileSettings);
/// \brief  Saves the program settings from file
/// \return  bool true if successful
bool saveProgramSettings(ProgramSettings& fileSettings);
