// author : Antonio Almeida
// date   : 06/12/2014
#pragma once
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Vectors.h"
#include "Matrix.h"
#include "Window.h"

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Vertex structure 
//----------------------------------------------------------------------------------------------------------------------
struct Vertex
{
	vec3 vertex;
};
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Vertex UV structure 
//----------------------------------------------------------------------------------------------------------------------
struct Vertex_UV
{
	vec3 vertex;
	vec2 uv;
};
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Vertex Normal UV structure 
//----------------------------------------------------------------------------------------------------------------------
struct Vertex_Normal_UV
{
	vec3 vertex;
	vec3 normal;
	vec2 uv;
};

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Loads a meshes file into the program
//----------------------------------------------------------------------------------------------------------------------
class ModelLoader
{
public:
	///\brief Constr. Loads Data into the Float and Uint Buffer
	///\prama Pointer to the File Name
	ModelLoader(const char* mesh);

	///\brief Destr
	~ModelLoader();
	
	///\brief draws the mesh
	void draw();

  void update(Matrix3* matrix);

	inline Matrix3 matrix()
	{ return m_matrix; }

	///\brief return the vertex normal and uv data
	///\return Vertex_Normal_UV vertices
	inline const std::vector<Vertex_Normal_UV>& vertices()
	{ return m_meshData; }
	
	///\brief return the indices
	///\return int indices
	inline const std::vector<int>& indices()
	{ return m_meshIndices; }


private:
  Matrix3 m_matrix;  ///<matrix for mesh
	std::vector<Vertex_Normal_UV> m_meshData; ///< vertex/normal/uv data
	std::vector<int> m_meshIndices; ///< mesh indices
	uint32_t m_vbo; ///< VBO
	uint32_t m_ibo; ///< IBO

	///\brief Loads data in to a String
	///\prama Pointer to the File Name
	///\prama String to load the Model into
	///\prama Store the size of the String
	void loadString(const char* file, std::string &returnData, int &returnSize);
};
