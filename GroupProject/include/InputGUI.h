// author : Lewis Ward
// date   : 21/11/2014
#pragma once
#include "EventHandler.h"
#include "Sound.h"
#include "Texture.h"
#include "modelLoader.h"
#include "gl/Program.h"
#include <vector>

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Repersents a button on a menu
//----------------------------------------------------------------------------------------------------------------------
class MenuButton
{
public:
	///\brief Constr
	///\prama vec2 origin of the button (top left)
	///\prama vec2 end of the button (bottom right)
	MenuButton(vec2 origin, vec2 end);
	///\brief Destr
	//~MenuButton();

	///\brief when a user clicks on a button
	///\prama vec2 mouse or Kinect joint
	bool clickedSelected(vec2& mPos);

private:
	vec2 m_origin; ///< top left of the button
	vec2 m_end; ///< bottom right of the button
};
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Repersents one menu 
//----------------------------------------------------------------------------------------------------------------------
class Menu
{
public:
	///\brief Constr
	Menu(vec2 screenSize);
	///\brief Destr
	~Menu();

	///\brief draw the menu
	void draw();
	///\brief listen for an event
	///\prama vec3 mouse position
	void listen(int& code,vec2 updateMouse, float dt);
	///\brief add a button to the menu
	///\prama vec2 origin of the button
	///\prama vec2 end of the button
	void addButton(vec2 o, vec2 e);
	///\brief remove a button from the menu
	void removeButton();
	///\brief query a button on the menu
	///\prama int button number 
	///\return MenuButton 
	inline MenuButton queryButton(int button)
	{ return m_buttons[button]; }
	///\brief returns the last button pressed
	///\return int last button pressed
	inline int getButtonPressed()
	{ return m_lastButtonPressed; }
	///\brief resets the last button pressed
	inline void resetButtonPressed()
	{ m_lastButtonPressed = 100; }
	///\brief set the size of the menu
	///\prama vec2 menu size
	inline void menuSize(vec2 windowSize)
	{ m_screenSize = windowSize; }
	///\brief set the mouse position to the players hand
	///\prama vec2 players hand
	inline void setKinectMouse(vec2 mouse)
	{ m_kinectMouse = mouse; }
	///\brief set the state of the Kinect
	///\prama bool is connected or not
	inline void connectedKinect(bool kinectState)
	{ m_kinectConnected = kinectState; }

private:
	uint32_t m_vbo;
	uint32_t m_ibo;
	vec2 m_kinectMouse;
	bool m_kinectConnected;
	float m_buttonTimeCounter;
	std::vector<MenuButton> m_buttons; ///< vector of buttons
	std::vector<int> m_listenCode; ///< event codes
	int m_lastButtonPressed; ///< what was the last button pressed
	vec2 m_screenSize; ///< size of the screen/menu
};
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Handles all inputs and HUDS/GUI within the program. Also called 'IGUI'
//----------------------------------------------------------------------------------------------------------------------
class InputGUI
{
public:
	///\brief Constr
	InputGUI();
	///\brief Destr
	~InputGUI();

	///\brief update all inputs and everything GUI
	///\prama	bool gameloop so IGUI can close program
	///\return int eventCode
	int update(bool& gameloop, float dt);
	///\brief draw anything GUI, is called by update method
	void draw();

	///\brief set which menu you want to have active
	///\prama	int index of active menu
	///\prama	vec2 size of the screen
	void activeMenu(int index, vec2 screenSize);
	///\brief add a menu
	///\prama	uint32_t* texture for menu
	void addMenu(vec2 size);
	///\brief add a button to the menu
	///\prama vec2 origin of the button
	///\prama vec2 end of the button
	void addMenuButton(vec2 origin, vec2 end);
	///\brief remove a button from the menu
	void removeMenuButton();
	///\brief lets you edit a button
	///\prama int index of button
	///\prama vec2 new origin
	void editMenuButton(int menuIndex, int buttonIndex, vec2 o);
	///\brief get the last button pressed on active menu
	///\return int button pressed
	int lastMenuButtonPress();
	///\brief resets the last button pressed
	void resetLastButtonPressed();
	///\brief get the current menu
	///\return int active menu
	inline int currentMenu()
	{ return m_currentMenu; }
	///\brief set the mouse position to the players hand
	///\prama vec2 players hand
	inline void setMouse(vec2 mouse)
	{ m_menus[m_currentMenu].setKinectMouse(mouse); }
	///\brief set the state of the Kinect
	///\prama bool is connected or not
	void kinectState(bool state);

private:
	std::vector<Menu> m_menus; ///< vector of buttons
	EHandler m_GUIEventHandler; ///< event handler for IGUI
	int m_currentMenu; ///< what is the current menu being used
};
