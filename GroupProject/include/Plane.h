// author : Jay Collins
// date   : 17/12/2014

#pragma once
#include "InputGUI.h"
#include "Matrix.h"

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Plane Handler
//----------------------------------------------------------------------------------------------------------------------
class Plane
{
public:
	///\brief Constr
	///\prama float window height
	///\prama float window width
	Plane(float height, float width);
	///\brief Destr
	~Plane();

  ///\brief update function
	///\prama int event code
	///\prama float delta time
	///\return bool game still in play or not
	bool update(int input, float dt);

	///\brief update function (Kinect version)
	///\prama float delta time
	///\prama vec2 player left hand
	///\prama vec2 player right hand
	///\return bool game still in play or not

	bool update(float dt, vec2 leftHand, vec2& rightHand);

  /////\brief draw function
  void draw();

	///\brief get the score
	///\return int score
	inline int score()
	{ return m_score; }

private:
  float m_height; ///< window height
  float m_width; ///< window width
  ModelLoader* m_mesh[3];///< Plane Mesh
  Texture* m_texture[3];///< Plane Texture
	Matrix3 m_Planetransform;//< Matrix for plane
  ogl::Program m_program; ///< shader program
  ogl::VertexShader* m_vShader; ///< vertex shader
  ogl::FragmentShader* m_fShader; ///< fragment shader
	int m_score; ///< player score
	bool m_hitRing[3]; ///< keep track if player hit a ring
	float m_fuel; ///< plane fuel
	Sound* m_sound; ///< background music
};
