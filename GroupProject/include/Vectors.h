#pragma once
#include <cmath>
//----------------------------------------------------------------------------------------------------------------------
/// \brief  vec2 struct
//----------------------------------------------------------------------------------------------------------------------
struct vec2
{
	float x; ///< x component
	float y; ///< y component

	vec2() : x(0.0f), y(0.0f) {}; ///< sets x and y to zero
	vec2(float i) : x(i), y(i) {}; ///< sets both x and y to the same value 
	vec2(float a, float b) : x(a), y(b) {}; ///< sets x and y to differnet values

	// operator overloading
	inline const vec2 operator = (vec2 a)
	{	x = a.x; y = a.y; return *this; }
	inline const vec2 operator += (vec2 a)
	{	x += a.x; y += a.y; return *this; }
	inline const vec2 operator -= (vec2 a)
	{	x -= a.x; y -= a.y; return *this; }
	inline const vec2 operator *= (vec2 a)
	{	x *= a.x; y *= a.y; return *this; }
	inline const vec2 operator /= (vec2 a)
	{	x /= a.x; y /= a.y; return *this; }
};
// add two vectors together
inline vec2 operator + (vec2 a, vec2 b)
{ 
	vec2 t; 
	t.x = a.x + b.x; 
	t.y = a.y + b.y;
	return t;
}
// subtract two vectors
inline vec2 operator - (vec2 a, vec2 b)
{ 
	vec2 t; 
	t.x = a.x - b.x; 
	t.y = a.y - b.y;
	return t;
}
// multiplies vector by float (scalar)
inline vec2 operator * (vec2 a, float s)
{ 
	vec2 t; 
	t.x = a.x * s; 
	t.y = a.y * s;
	return t;
}
// multiplies float (scalar) by vector
inline vec2 operator * (float s, vec2 a)
{ 
	vec2 t; 
	t.x = a.x * s; 
	t.y = a.y * s;
	return t;
}
// divides vector by float (scalar)
inline vec2 operator / (vec2 a, float s)
{ 
	vec2 t; 
	t.x = a.x / s; 
	t.y = a.y / s;
	return t;
}
// dot product of two vectors, if you enter the same vector for both parameters you will get the squared length
inline float dot(vec2 a, vec2 b)
{ return (a.x * b.x) + (a.y * b.y); }
// the length of a vector
inline float length(vec2 a)
{ return std::sqrt(dot(a, a)); }
// normalize the vector to a unit length
inline vec2 normalize(vec2 a)
{
	// get the length of the vector
	float l = length(a);
	// absolute value of the length tested to stop division by zero
	if(std::fabs(l) > 0.000001)
	{
		return vec2(a.x /= l, a.y /= l);
	}
	else
	{
		return vec2(a.x * 0, a.y * 0);
	}
}
/// unary negation
inline vec2 operator - (vec2 a)
{
  vec2 r;
  r.x = -a.x;
  r.y = -a.y;
  return r;
}
//----------------------------------------------------------------------------------------------------------------------
/// \brief  vec3 struct
//----------------------------------------------------------------------------------------------------------------------
struct vec3
{
	float x; ///< x component
	float y; ///< y component
	float z; ///< z component
	float w; ///< w component

	vec3() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}; ///< sets x, y and z to zero
	vec3(float i) : x(i), y(i), z(i), w(0.0f) {}; ///< sets x, y and z to the same value 
	vec3(float a, float b, float c) : x(a), y(b), z(c), w(0.0f) {}; ///< sets x, y and z to differnet values
	vec3(vec2 v) : x(v.x), y(v.y), z(0.0f), w(0.0f) {}; ///< takes a vec2 for x and y and sets z to zero
	vec3(vec2 v, float i) : x(v.x), y(v.y), z(i), w(0.0f) {}; ///< takes a vec2 for x and y and set z
	
	// operator overloading
	inline const vec3 operator = (vec3 a)
	{	x = a.x; y = a.y; z = a.z; return *this; }
	inline const vec3 operator += (vec3 a)
	{	x += a.x; y += a.y; z += a.z; return *this; }
	inline const vec3 operator -= (vec3 a)
	{	x -= a.x; y -= a.y; z -= a.z; return *this; }
	inline const vec3 operator *= (vec3 a)
	{	x *= a.x; y *= a.y; z *= a.z; return *this; }
	inline const vec3 operator /= (vec3 a)
	{	x /= a.x; y /= a.y; z /= a.z; return *this; }
};
// add two vectors together
inline vec3 operator + (vec3 a, vec3 b)
{ 
	vec3 t; 
	t.x = a.x + b.x; 
	t.y = a.y + b.y;
	t.z = a.z + b.z;
	return t;
}
// subtract two vectors
inline vec3 operator - (vec3 a, vec3 b)
{ 
	vec3 t; 
	t.x = a.x - b.x; 
	t.y = a.y - b.y;
	t.z = a.z - b.z;
	return t;
}
// multiplies vector by float (scalar)
inline vec3 operator * (vec3 a, float s)
{ 
	vec3 t; 
	t.x = a.x * s; 
	t.y = a.y * s;
	t.z = a.z * s;
	return t;
}
// multiplies float (scalar) by vector
inline vec3 operator * (float s, vec3 a)
{ 
	vec3 t; 
	t.x = a.x * s; 
	t.y = a.y * s;
	t.z = a.z * s;
	return t;
}
// divides vector by float (scalar)
inline vec3 operator / (vec3 a, float s)
{ 
	vec3 t; 
	t.x = a.x / s; 
	t.y = a.y / s;
	t.z = a.z / s;
	return t;
}
// dot product of two vectors
inline float dot(vec3 a, vec3 b)
{ return (a.x * b.x) + (a.y * b.y) + (a.z * b.z); }
// cross product of two vectors
inline vec3 cross(vec3 a, vec3 b)
{
	vec3 t;
	t.x = a.y * b.z - a.z * b.y;
	t.y = a.z * b.x - a.x * b.z;
	t.z = a.x * b.y - a.y * b.x;
	return t;
}
// the length of a vector
inline float length(vec3 a)
{ return std::sqrt(dot(a, a)); }
// normalize the vector to a unit length
inline vec3 normalize(vec3 a)
{
	// get the length of the vector
	float l = length(a);
	// // absolute value of the length tested to stop division by zero
	if(std::fabs(l) > 0.000001)
	{
		return vec3(a.x /= l, a.y /= l, a.z /= l);
	}
	else
	{
		return vec3(a.x * 0, a.y * 0, a.z * 0);
	}
}
/// unary negation
inline vec3 operator - (const vec3& a)
{
  vec3 r;
  r.x = -a.x;
  r.y = -a.y;
  r.z = -a.z;
  return r;
}
