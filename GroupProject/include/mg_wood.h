// author : Antonio Almeida
// date   : 04/01/2014

#pragma once
#include "Axe.h"

//----------------------------------------------------------------------------------------------------------------------
/// \brief  mg_Plane Handler
//----------------------------------------------------------------------------------------------------------------------
class mg_wood
{
public:
	///\brief Constr
	mg_wood(float height, float width);
	///\brief Destr
	~mg_wood();
	
	///\brief update function
	///\prama int event code
	///\prama float delta time
	///\return bool game still in play or not
	bool update(int& EventCode, float& dt);
	
	///\brief update function
	///\prama float delta time
	///\prama vec2 player left hand
	///\prama vec2 player right hand
	///\return bool game still in play or not
	bool update(float& dt, vec2& kinectLeftHand, vec2& kinectRightHand);
  
	///\brief draw function
  void draw();

	///\brief get the score
	///\return int score
	int gameScore();

private:
  Axe* m_player;///< Ptr to plane class
};
