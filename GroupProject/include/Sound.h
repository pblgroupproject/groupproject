// author : Jay Collins
// date   : 1/12/2014
//  Code based on https://gist.github.com/armornick/3447121
#pragma once
#include "Window.h"

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Sound Handler
//----------------------------------------------------------------------------------------------------------------------
class Sound
{
public:
	///\brief Constr
	Sound(const char* filename);
	///\brief Destr
	~Sound();
  /////\brief Start the audio player, This will load the SDL audio player and begin playing the .wav file
  void Start();
  /////\brief Play the .wav, Will continue playing the .wav when it's paused
  void Play();
  /////\brief pause the .wav, Will pause the playing .wav
  void Pause();
  /////\brief Stop audio, Will stop the .wav playing and close the audio player (if you want to start playing .wav again the Start method must be called to open the audio player again) 
  void Stop();

private:
	SDL_AudioSpec m_wav_sound;///< .wav file structure
  SDL_AudioDeviceID m_dev;///< I.D of audio device used to playback
  Uint32 m_wav_length;///< length of .wav file (used to update .wav position during playback)
  Uint8 *m_wav_buffer;///< buffer of the .wav file (used to update .wav position during playback
};
