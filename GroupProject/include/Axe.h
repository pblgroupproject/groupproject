// author : Antonio Almeida
// date   : 04/01/2014

#pragma once
#include "InputGUI.h"
#include "Matrix.h"

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Axe Handler
//----------------------------------------------------------------------------------------------------------------------
class Axe
{
public:
	///\brief Constr
	Axe(float height, float width);
	///\brief Destr
	~Axe();

	///\brief update function
	///\prama int event code
	///\prama float delta time
	///\return bool game still in play or not
	bool update(int input, float dt);

	///\brief update function (Kinect version)
	///\prama float delta time
	///\prama vec2 player left hand
	///\prama vec2 player right hand
	///\return bool game still in play or not

	bool update(float dt, vec2 leftHand, vec2& rightHand);

  /////\brief draw function
  void draw();

	///\brief get the score
	///\return int score
	inline int score()
	{ return m_score; }

private:
  float m_height; ///< window height
  float m_width; ///< window width
  vec3 m_position;///< Position of Axe
  ModelLoader* m_mesh[4];///< Axe Mesh
  Texture* m_texture[3];///< Axe Texture
	Matrix3 m_Axetransform;//< Matrix for Axe
  ogl::Program m_program; ///< shader program
  ogl::VertexShader* m_vShader; ///< vertex shader
  ogl::FragmentShader* m_fShader; ///< fragment shader
	int m_score; ///< player score
	float m_timer; ///< player score
	bool m_hitLog[3]; ///< has the player hit the log
	bool m_axeLifted[2]; ///< has the player lifted the axe after hitting the log
	bool m_logAnimation; ///< is the animation running or not
	Sound* m_sound; ///< background music
};
