// author : Lewis Ward
// date   : 21/11/2014
#pragma once
#include "Window.h"
#include "Xkinect.h"

// event code that the event handler will output
enum eventCodes
{
	eNoEvent, // default, no event triggered
	eQuit, // user quit
	e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, // 0 - 9 number keys
	eLA, // left alt
	eLC, // left crtl
	eRA, // right alt
	eRC, // right crtl
	eW, eA, eS, eD, eSpace, eUp, eDown, eLeft, eRight, // W/A/S/D, space left/right/up/down arrows
	eMMotion, // mouse motion
	eMBDownL, eMBDownM, eMBDownR, // button down left/middle/right
	eMBUpL, eMBUpM, eMBUpR, //button up left/middle/right
};

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Event Handler
//----------------------------------------------------------------------------------------------------------------------
class EHandler
{
public:
	///\brief Constr
	EHandler();
	///\brief Destr
	~EHandler();

	///\brief update the handler, will listen for events and return event code when event triggered
	///\return int the enum of the event code that has been triggered
	int update();

	///\brief get current mouse position
	///\return vec2 mouse position
	inline vec2 mousePosition()
	{ return m_mouseCoords; }

private:
	SDL_Event m_events;
	vec2 m_mouseCoords;
};

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Delta Time (dt)
//----------------------------------------------------------------------------------------------------------------------
class DeltaTime
{
public:
	///\brief Constr
	DeltaTime();
	///\brief Destr
	~DeltaTime();

	///\brief update delta time
	///\return float delta time
	float update();

private:
	uint64_t m_startFrame; ///< starting frame
	uint64_t m_lastFrame; ///< last frame
	uint64_t m_newFrame; ///< new frame
	float m_secondsCount; ///< counts per second
};
