// author : Lewis Ward
// date   : 22/10/2014

#pragma once
#include "Window.h"
#include "modelLoader.h"
#include <Windows.h>
#include <Ole2.h>
#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h> 
#define KIN_WIDTH 640 ///< width of the kinect image from colour camera
#define KIN_HEIGHT 480 ///< height of the kinect image from colour camera

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Xbox 360 Kinect sensor class
//----------------------------------------------------------------------------------------------------------------------
class kinectSensor
{
public:
	///\brief Constr
	kinectSensor();
	///\brief Destr
	~kinectSensor();

	///\brief Draw the hand position (current set for right hand only)
	void draw();

	///\brief setup the Kinect returns if it's been setup for not (i.e not connected!)
	///\return bool Kinect connected or not
	bool sensorInit(vec2 screenRatio);

	///\brief get the next frame from the Kinect
	///\parma GLubyte* pass in the current frame to update it to the next frame
	void nextFrameData(GLubyte* nextFrame);

	///\brief get the next frame from the Kinect (depth mode)
	///\parma GLubyte* pass in the current frame to update it to the next frame
	void nextDepthFrameData(GLubyte* nextFrame);

	///\brief process the skeleton data the Kinect detects
	void processSkeletonData();

	///\brief get the right hand joint
	///\return vec2 joint
	inline vec2 rightHand()
	{ return m_rightHand; }

	///\brief get the left hand joint
	///\return vec2 joint
	inline vec2 leftHand()
	{ return m_leftHand; }

	///\brief get the state of the kinect
	///\return bool state
	inline bool state()
	{ return m_sensorDetected; }

private:
	vec2 m_screenRatio; ///< window ratio
	HANDLE m_rgbCamera; ///< colour camera
	HANDLE m_depthCamera; ///< depth camera
	HANDLE m_skeletonTrack; ///< depth camera
	vec2 m_rightHand; ///< right hand position
	vec2 m_leftHand; ///< left hand position
	INuiSensor* m_sensor; ///< structure for the sensor
	bool m_sensorDetected; ///< keeps track if a sensor has been detected or not

	/// \brief draw the bones of each joint from the skeleton data
	/// \parma NUI_SKELETON_DATA skeleton data (i.e joint your tracking)
	void drawSkeleton(const NUI_SKELETON_DATA& skeleton);

	/// \brief convert joint from 3D space (i.e. arm the Kinect has detected) to 2D screen space
	/// \parma Vector4 Microsoft's Vector4 data type for the location of the joint 
	/// \return vec2 screen location
	vec2 toScreen(Vector4 jointLocation);

	/// \brief draws a bone to screen
	/// \parma NUI_SKELETON_DATA the skeleton data
	/// \parma NUI_SKELETON_POSITION_INDEX the joint one index (head, kip, knee etc.)
	/// \parma NUI_SKELETON_POSITION_INDEX the joint two index (head, kip, knee etc.)
	void drawBone(const NUI_SKELETON_DATA& skeleton, NUI_SKELETON_POSITION_INDEX jOne, NUI_SKELETON_POSITION_INDEX jTwo);

	/// \brief next frame is ready
	/// \parma NUI_SKELETON_FRAME skeleton frame data
	void nextFrameReady(NUI_SKELETON_FRAME* sFrame);

	/// \brief draw the position of the joint in screen space
	/// \parma Vector4 Microsoft's Vector4 data type for the location of the joint 
	void drawPosition(Vector4 position);

	/// \brief draws two bones to screen with a line between them
	/// \parma Vector4 Microsoft's Vector4 data type for joint A
	/// \parma Vector4 Microsoft's Vector4 data type for joint B
	void drawBonesLines(Vector4 jointA, Vector4 jointB);
};
