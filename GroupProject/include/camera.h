// author : Antonio Almeida
// date   : 18/12/2014
#pragma once
#include "Vectors.h"
#include "EventHandler.h"

//----------------------------------------------------------------------------------------------------------------------
/// \brief  Camera
//----------------------------------------------------------------------------------------------------------------------
class camera
{
public:
	///\brief Constr
	camera(int w, int h);

	///\brief Constr
	camera(int w, int h, vec3 pos, vec3 dir, vec3 up);

	///\brief Render
	void render();

	///\brief Keyboard Control
	///\return Bool
	bool keyboard();

	///\brief Retrieve Up Variable
	///\return Vec3 Up
	inline vec3 returnUp()
	{ return m_up; }

	///\brief Retrieve Direction Variable
	///\return Direction
	inline vec3 returnDir()
	{ return m_direction; }

	///\brief Retrieve Position Variable
	///\return Position
	inline vec3 returnPos()
	{ return m_position; }

	///\brief Modifies position based on current position
	inline void modifyPosition(vec3 a)
	{ m_position += a; }

	///\brief Sets a new position for the camera
	inline void setPosition(vec3 a)
	{ m_position = a; }

private:

	///\brief Initialise Quats on the Camera
	void Initialize();
	///\brief Update the Camera
	void Update();

	vec3 m_position; ///< Vec3 Position
	vec3 m_direction;	///< Vec3 Direction
	vec3 m_up; ///< Vec3 Up

	int m_wHeight; ///< Int Window Height
	int m_wWidth; ///< Int Window Width

	float m_h; ///< Horizontal Angle
	float m_v; ///< Vertical Angle

	EHandler m_Control; ///< Ehandler Keypress

	vec2 m_mousepos; ///< Vec2 Mouse Position
};
