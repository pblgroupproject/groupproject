// author : Lewis Ward
// date   : 09/12/2014
#pragma once
#include "Window.h"

namespace ogl
{
/// \brief get a gl error within shader
/// \prama const char* shader file
void glError(const char* file);
/// \brief load a shader from file, should be called before creating a shader
/// \prama const char* shader file
char* loadShaderFile(const char* shaderFilename);

//----------------------------------------------------------------------------------------------------------------------
/// \brief  GLSL Vertex Shader
//----------------------------------------------------------------------------------------------------------------------
class VertexShader
{
public:
	/// \brief Constr, create and compile shader
	/// \prama const char* shader source code
	VertexShader(const char* shaderSourceCode, const char* shaderFile);
	/// \brief Destr
	~VertexShader();

	/// \brief get the shader source code
	/// \return const char* shader source code
	const char* shaderSource() const;

	/// \brief get the shader object
	/// \return uint32_t shader object
	inline uint32_t shader() const
	{ return m_vShader; }

private:
	uint32_t m_vShader;
	std::string  m_vShaderFilename;
	std::string m_vShaderSource;
};
//----------------------------------------------------------------------------------------------------------------------
/// \brief  GLSL Fragment Shader
//----------------------------------------------------------------------------------------------------------------------
class FragmentShader
{
public:
	/// \brief Constr, create and compile shader
	/// \prama const char* shader source code
	FragmentShader(const char* shaderSourceCode, const char* shaderFile);
	/// \brief Destr
	~FragmentShader();

	/// \brief get the shader source code
	/// \prama const char* shader source code
	const char* shaderSource() const;

	/// \brief get the shader object
	/// \return uint32_t shader object
	inline uint32_t shader() const
	{ return m_fShader; }

private:
	uint32_t m_fShader;
	std::string m_fShaderFilename;
	std::string m_fShaderSource;
};
////////////////////////////
}; // end of ogl namespace
////////////////////////////
