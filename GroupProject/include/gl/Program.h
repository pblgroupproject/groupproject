// author : Lewis Ward
// date   : 09/12/2014
#pragma once
#include "Shader.h"

namespace ogl
{
//----------------------------------------------------------------------------------------------------------------------
/// \brief  Program for shader
//----------------------------------------------------------------------------------------------------------------------
class Program
{
public:
	/// \brief Constr
	Program() : m_program(0){};
	/// \brief Destr
	~Program();

	bool create(const VertexShader* const vertex, const FragmentShader* const fragment);

	void bind() const;

	void unbind() const;

	inline uint32_t program()
	{ return m_program; }

private:
	uint32_t m_program;
};
////////////////////////////
}; // end of ogl namespace
////////////////////////////
